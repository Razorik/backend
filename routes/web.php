<?php

Route::group(['prefix' => 'admin'], function() {
    Route::get('/', 'Backend\MainController@index')->middleware('auth')->name('dashboard');
    Route::get('/login', function() {
        return view('admin.auth.login', ['title' => 'Авторизация']);
    })->name('login');
    Route::post('/login', 'Auth\LoginController@login');
    Route::get('/logout', 'Auth\LoginController@logout');
    Route::resource('/settings', 'Backend\SettingController', ['except' => ['show']])->middleware('accessCheck:950');
    Route::resource('/manage', 'Backend\TableController', ['except' => ['show']])->middleware('accessCheck:1000');
    Route::resource('/manage/{table_id}/fields', 'Backend\FieldController', ['except' => ['show', 'index']])->middleware('accessCheck:1000');
    Route::resource('/users', 'Backend\UserController', ['except' => ['show']])->middleware('accessCheck:950');
    Route::resource('/roles', 'Backend\RoleController', ['except' => ['show']])->middleware('accessCheck:950');
    Route::resource('/groups', 'Backend\GroupController', ['except' => ['show']])->middleware('accessCheck:950');
    Route::resource('/permissions', 'Backend\PermissionController', ['except' => ['show']])->middleware('accessCheck:950');
    /**
     * Доп роуты для пользователей
     */
    Route::get('/users/{userId}/roles/add', 'Backend\RoleController@formAdd')->middleware('accessCheck:950');
    Route::get('/users/{userId}/roles/{roleId}/change', 'Backend\RoleController@formChange')->middleware('accessCheck:950');
    Route::get('/users/{userId}/roles/{roleId}/delete', 'Backend\RoleController@roleDelete')->middleware('accessCheck:950');
    Route::post('/users/{userId}/roles/add', 'Backend\RoleController@roleAdd')->middleware('accessCheck:950');
    Route::put('/users/{userId}/roles/{roleId}/change','Backend\RoleController@roleChange')->middleware('accessCheck:950');
    Route::get('/users/{userId}/groups/add', 'Backend\GroupController@formAdd')->middleware('accessCheck:950');
    Route::get('/users/{userId}/groups/{roleId}/change', 'Backend\GroupController@formChange')->middleware('accessCheck:950');
    Route::get('/users/{userId}/groups/{roleId}/delete', 'Backend\GroupController@groupDelete')->middleware('accessCheck:950');
    Route::post('/users/{userId}/groups/add', 'Backend\GroupController@groupAdd')->middleware('accessCheck:950');
    Route::put('/users/{userId}/groups/{groupId}/change','Backend\GroupController@groupChange')->middleware('accessCheck:950');
    /**
     * Доп. роуты для групп
     */
    Route::get('/{entityType}/{entityId}/permissions/add', 'Backend\PermissionController@addPermissionForm')->middleware('accessCheck:950');
    Route::post('/{entityType}/{entityId}/permissions/add', 'Backend\PermissionController@addPermission')->middleware('accessCheck:950');
    Route::get('/{entityType}/{entityId}/permissions/{permissionId}/change', 'Backend\PermissionController@editPermissionForm')->middleware('accessCheck:950');
    Route::post('/{entityType}/{entityId}/permissions/{permissionId}/change', 'Backend\PermissionController@editPermission')->middleware('accessCheck:950');
    Route::get('/{entityType}/{entityId}/permissions/{permissionId}/delete', 'Backend\PermissionController@deletePermission')->middleware('accessCheck:950');

    foreach(\App\Table::all() as $table) {
        Route::resource('/' . $table->name, 'Backend\\' . (strlen($table->custom_controller) > 0 ? $table->custom_controller : 'ModuleController'))->middleware('accessCheck:' . $table->access_level or 0);
    }
});

Route::get('/', function() {
    return 'Сайт в разработке';
});
