@extends('admin.groups.html')
@section('center-settings')
    @include('admin.layouts.common.title-box', ['title' => isset($group) ? 'Изменение группы ' . $group->name : 'Создание новой группы', 'titleSmall' => isset($group) ? 'Группа ' . $group->name : 'Заполните форму для добавления новой группы.'])
    <div class="row padding">
        <form action="/admin/groups{{ isset($group) ? '/' . $group->id : '' }}" method="POST">
            <div class="form-group row">
                <label class="col-sm-2 form-control-label">Название группы</label>
                <div class="col-sm-10">
                    <input name="name" type="text" value="{{ isset($group) ? $group->name : '' }}" class="form-control"
                           placeholder="Введите название группы" required>
                </div>
            </div>
            {{ csrf_field() }}
            @if(isset($group))
                <input type="hidden" name="_method" value="put">
            @endif
            <div class="form-group row m-t-lg">
                <div class="col-sm-4 col-sm-offset-2">
                    <a class="btn white" href="/admin/groups">Отмена</a>
                    <button type="submit" class="btn btn-primary">Сохранить</button>
                </div>
            </div>
        </form>
        @if(isset($group))
        <div class="box-body">
            Search: <input id="filter" type="text" class="form-control input-sm w-auto inline m-r"/>
            <a class="btn btn-icon white" href="/admin/groups/{{ $group->id }}/permissions/add">
                <i class="fa fa-plus"></i>
            </a>
        </div>
        <div>
            <table class="table m-b-none" data-ui-jp="footable" data-filter="#filter" data-page-size="5">
                <thead>
                <tr>
                    <th data-toggle="true">
                        ID
                    </th>
                    <th>
                        Ключ
                    </th>
                    <th>
                        Уровень доступа
                    </th>
                    <th>
                        Управление
                    </th>
                </tr>
                </thead>
                <tbody>
                @foreach($group->permissions as $permission)
                    <tr>
                        <td>{{ $permission->id }}</td>
                        <td><a href="/admin/groups/{{ $group->id }}/permissions/{{ $permission->id }}/change">{{ $permission->key }}</a></td>
                        <td>
                            {{ $permission->value }}
                        </td>
                        <td class="manage">
                            <a class="btn btn-icon white" href="/admin/groups/{{ $group->id }}/permissions/{{ $permission->id }}/change">
                                <i class="ion-edit"></i>
                            </a>
                            <a class="btn btn-icon white" href="/admin/groups/{{ $group->id }}/permissions/{{ $permission->id }}/delete">
                                <i class="ion-trash-a"></i>
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
                <tfoot class="hide-if-no-paging">
                <tr>
                    <td colspan="5" class="text-center">
                        <ul class="pagination">
                        </ul>
                    </td>
                </tr>
                </tfoot>
            </table>
        </div>
        @endif
    </div>
@endsection