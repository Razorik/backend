@extends('admin.layouts.template.content')

@section('content')
    <div class="padding">
        <div class="navbar">
            <div class="pull-center">
                <a href="/admin" class="navbar-brand">
                    <div data-ui-include="'{{ asset('backend/images/logo.svg') }}'"></div>
                    <img src="{{ asset('backend/images/logo.png') }}" alt="." class="hide">
                    <span class="hidden-folded inline">{{ env('APP_NAME') }} admin panel</span>
                </a>
            </div>
        </div>
    </div>
    <div class="b-t">
        <div class="center-block w-xxl w-auto-xs p-y-md text-center">
            <div class="p-a-md">
                <form name="form" action="/admin/login" method="POST">
                    <div class="form-group">
                        <input name="email" type="email" class="form-control" placeholder="Email" required>
                    </div>
                    <div class="form-group">
                        <input name="password" type="password" class="form-control" placeholder="password" required>
                    </div>
                    <div class="m-b-md">
                        <label class="md-check">
                            <input type="checkbox"><i class="primary"></i> Keep me signed in
                        </label>
                    </div>
                    {{ csrf_field() }}
                    <button type="submit" class="btn btn-lg black p-x-lg">Sign in</button>
                </form>
            </div>
        </div>
    </div>
@endsection