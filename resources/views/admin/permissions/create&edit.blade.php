@extends('admin.permissions.html')
@section('center-settings')
    @include('admin.layouts.common.title-box', ['title' => isset($permission) ? 'Изменение права доступа ' . $permission->key : 'Создание нового права доступа', 'titleSmall' => isset($permission) ? 'Право доступа ' . $permission->key : 'Заполните форму для добавления нового права доступа.'])
    <div class="row padding">
        <form action="/admin/permissions{{ isset($permission) ? '/' . $permission->id : '' }}" method="POST">
            <div class="form-group row">
                <label class="col-sm-2 form-control-label">Ключ права доступа</label>
                <div class="col-sm-10">
                    <input name="key" type="text" value="{{ isset($permission) ? $permission->key : '' }}" class="form-control"
                           placeholder="Введите название ключа права доступа" required>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 form-control-label">Уровень права доступа</label>
                <div class="col-sm-10">
                    <input name="value" type="text" value="{{ isset($permission) ? $permission->value : '' }}" class="form-control"
                           placeholder="Введите уровень права доступа (целое число)" required>
                </div>
            </div>
            {{ csrf_field() }}
            @if(isset($permission))
                <input type="hidden" name="_method" value="put">
            @endif
            <div class="form-group row m-t-lg">
                <div class="col-sm-4 col-sm-offset-2">
                    <a class="btn white" href="/admin/permissions">Отмена</a>
                    <button type="submit" class="btn btn-primary">Сохранить</button>
                </div>
            </div>
        </form>
    </div>
@endsection