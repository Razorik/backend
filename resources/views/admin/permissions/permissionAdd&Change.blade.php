@extends('admin.users.html')
@section('center-settings')
    @include('admin.layouts.common.title-box', ['title' => isset($permission) ? 'Изменение права доступа ' . $permission->key . ' в модуле ' . $entityName . ' у записи ' . $entity->name : 'Добавление прав доступа в модуле ' . $entityName . ' для записи ' . $entity->name, 'titleSmall' => isset($role) ? 'Запись ' . $entity->name : 'Заполните форму для добавления прав доступа'])
    <div class="padding">
        <form action="/admin/{{ $entityType }}/{{ $entity->id }}/permissions/{{ isset($permission) ? $permission->id . '/change' : 'add' }}" method="POST">
            <div class="form-group row">
                <label for="single" class="col-sm-2 form-control-label">Право доступа</label>
                <div class="col-sm-10">
                    <select id="single" class="form-control select2" data-ui-jp="select2" name="permission_id" data-ui-options="{theme: 'bootstrap'}">
                        @foreach($permissions as $onePermission)
                                <option value="{{ $onePermission->id }}" {{ isset($permission) ? $onePermission->id == $permission->id ? 'selected' : '' : '' }}>{{ $onePermission->key }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            {{ csrf_field() }}
            <div class="form-group row m-t-lg">
                <div class="col-sm-4 col-sm-offset-2">
                    <a class="btn white" href="/admin/{{ $entityType }}/{{ $entity->id }}/edit">Отмена</a>
                    <button type="submit" class="btn btn-primary">Сохранить</button>
                </div>
            </div>
        </form>
    </div>
@endsection