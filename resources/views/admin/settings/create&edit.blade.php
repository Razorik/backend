@extends('admin.settings.html')
@section('center-settings')
    @include('admin.layouts.common.title-box', ['title' => isset($setting) ? 'Изменение настройки ' . $setting->name : 'Создание новой настройки', 'titleSmall' => isset($setting) ? 'Настройка ' . $setting->name . ' в группе ' . $setting->group : 'Заполните форму для добавления новой настройки.'])
    <div class="padding">
        <form action="/admin/settings{{ isset($setting) ? '/' . $setting->key : '' }}" method="POST">
            <div class="form-group row">
                <label class="col-sm-2 form-control-label">Название</label>
                <div class="col-sm-10">
                    <input name="name" type="text" value="{{ isset($setting) ? $setting->name : '' }}" class="form-control" placeholder="Введите название настройки" required>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 form-control-label">Ключ</label>
                <div class="col-sm-10">
                    <input name="key" type="text" value="{{ isset($setting) ? $setting->key : '' }}" class="form-control" placeholder="Введите ключ настройки" required>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 form-control-label">Значение</label>
                <div class="col-sm-10">
                    <input name="value" type="text" value="{{ isset($setting) ? $setting->value : '' }}" class="form-control" placeholder="Введите значение настройки" required>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 form-control-label">Группа</label>
                <div class="col-sm-10">
                    <input name="group" type="text" value="{{ isset($setting) ? $setting->group : '' }}" class="form-control" placeholder="Введите группу настройки" required>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 form-control-label">Описание</label>
                <div class="col-sm-10">
                    <textarea name="description" class="form-control" rows="5">{{ isset($setting) ? $setting->description : '' }}</textarea>
                </div>
            </div>
            {{ csrf_field() }}
            @if(isset($setting))
                <input type="hidden" name="_method" value="put">
            @endif
            <div class="form-group row m-t-lg">
                <div class="col-sm-4 col-sm-offset-2">
                    <a class="btn white" href="/admin/settings">Отмена</a>
                    <button type="submit" class="btn btn-primary">Сохранить</button>
                </div>
            </div>
        </form>
    </div>
@endsection