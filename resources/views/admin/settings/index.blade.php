@extends('admin.settings.html')
@section('center-settings')
    @include('admin.layouts.common.title-box')
    <div class="box-body">
        Search: <input id="filter" type="text" class="form-control input-sm w-auto inline m-r"/>
        <a class="btn btn-icon white" href="/admin/settings/create">
            <i class="fa fa-plus"></i>
        </a>
    </div>
    <div>
        <table class="table m-b-none" data-ui-jp="footable" data-filter="#filter" data-page-size="5">
            <thead>
            <tr>
                <th data-toggle="true">
                    ID
                </th>
                <th>
                    Название
                </th>
                <th>
                    Ключ
                </th>
                <th data-hide="phone,tablet">
                    Значение
                </th>
                <th data-hide="phone,tablet" data-name="Группа">
                    Группа
                </th>
                <th>
                    Управление
                </th>
            </tr>
            </thead>
            <tbody>
            @foreach($settings as $setting)
                <tr>
                    <td>{{ $setting->id }}</td>
                    <td><a href="/admin/settings/{{ $setting->key }}/edit">{{ $setting->name }}</a></td>
                    <td>{{ $setting->key }}</td>
                    <td>{{ $setting->value }}</td>
                    <td>{{ $setting->group }}</td>
                    <td class="manage">
                        <a class="btn btn-icon white" href="/admin/settings/{{ $setting->key }}/edit">
                            <i class="ion-edit"></i>
                        </a>
                        <form action="/admin/settings/{{ $setting->key }}" method="POST">
                            <input type="hidden" name="_method" value="delete">
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-icon white">
                                <i class="ion-trash-a"></i>
                            </button>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
            <tfoot class="hide-if-no-paging">
            <tr>
                <td colspan="5" class="text-center">
                    <ul class="pagination">
                    </ul>
                </td>
            </tr>
            </tfoot>
        </table>
    </div>
@endsection