@extends('admin.users.html')
@section('center-settings')
    @include('admin.layouts.common.title-box', ['title' => isset($group) ? 'Изменение группы ' . $group->name . ' у пользователя ' . $user->name : 'Добавление новой группы у ' . $user->name, 'titleSmall' => isset($role) ? 'Группа ' . $group->name : 'Заполните форму для добавления новой группы пользователю.'])
    <div class="padding">
        <form action="/admin/users/{{ $user->id }}/groups/{{ isset($group) ? $group->id . '/change' : 'add' }}" method="POST">
            <div class="form-group row">
                <label for="single" class="col-sm-2 form-control-label">Группа</label>
                <div class="col-sm-10">
                    <select id="single" class="form-control select2" data-ui-jp="select2" name="group_id" data-ui-options="{theme: 'bootstrap'}">
                        @foreach($groups as $oneGroup)
                            <option value="{{ $oneGroup->id }}" {{ isset($group) ? $oneGroup->id == $group->id ? 'selected' : '' : '' }}>{{ $oneGroup->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            {{ csrf_field() }}
            @if(isset($group))
                <input type="hidden" name="_method" value="put">
            @endif
            <div class="form-group row m-t-lg">
                <div class="col-sm-4 col-sm-offset-2">
                    <a class="btn white" href="/admin/users/{{ $user->id }}/edit">Отмена</a>
                    <button type="submit" class="btn btn-primary">Сохранить</button>
                </div>
            </div>
        </form>
    </div>
@endsection