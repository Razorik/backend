@extends('admin.users.html')
@section('center-settings')
    @include('admin.layouts.common.title-box', ['title' => isset($user) ? 'Изменение пользователя ' . $user->name : 'Создание нового пользователя', 'titleSmall' => isset($user) ? 'Пользователь ' . $user->name : 'Заполните форму для добавления нового пользователя.'])
    <div class="row padding">
        <form action="/admin/users{{ isset($user) ? '/' . $user->id : '' }}" method="POST">
            <div class="form-group row">
                <label class="col-sm-2 form-control-label">Имя пользователя</label>
                <div class="col-sm-10">
                    <input name="name" type="text" value="{{ isset($user) ? $user->name : '' }}" class="form-control"
                           placeholder="Введите имя пользователя" required {{ $editable }}>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 form-control-label">Email пользователя</label>
                <div class="col-sm-10">
                    <input name="email" type="text" value="{{ isset($user) ? $user->email : '' }}" class="form-control"
                           placeholder="Введите email пользователя" required {{ $editable }}>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 form-control-label">Пароль</label>
                <div class="col-sm-10">
                    <input name="password" type="password" value="" class="form-control"
                           placeholder="Введите пароль, чтобы изменить текущий." {{ $editable }}>
                </div>
            </div>
            {{ csrf_field() }}
            @if(isset($user))
                <input type="hidden" name="_method" value="put">
            @endif
            <div class="form-group row m-t-lg">
                <div class="col-sm-4 col-sm-offset-2">
                    <a class="btn white" href="/admin/users">Отмена</a>
                    <button type="submit" class="btn btn-primary" {{ $editable }}>Сохранить</button>
                </div>
            </div>
        </form>
        @if(isset($user) && getUserAccess($user_info) >= getUserAccess($user))
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <h3 class="text-center">Роли пользователя</h3>
                <div class="box-body">
                    Search: <input id="filter" type="text" class="form-control input-sm w-auto inline m-r"/>
                    <a class="btn btn-icon white" href="/admin/users/{{ $user->id }}/roles/add">
                        <i class="fa fa-plus"></i>
                    </a>
                </div>

                <table class="table m-b-none" data-ui-jp="footable" data-filter="#filter" data-page-size="5">
                    <thead>
                    <tr>
                        <th data-toggle="true">
                            ID
                        </th>
                        <th>
                            Название
                        </th>
                        <th>
                            Управление
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($user->roles as $role)
                        <tr>
                            <td>{{ $role->id }}</td>
                            <td>
                                <a href="/admin/users/{{ $user->id }}/roles/{{ $role->id }}/change">{{ $role->name }}</a>
                            </td>
                            <td class="manage">
                                <a class="btn btn-icon white"
                                   href="/admin/users/{{ $user->id }}/roles/{{ $role->id }}/change">
                                    <i class="ion-edit"></i>
                                </a>
                                <a class="btn btn-icon white"
                                   href="/admin/users/{{ $user->id }}/roles/{{ $role->id }}/delete">
                                    <i class="ion-trash-a"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                    <tfoot class="hide-if-no-paging">
                    <tr>
                        <td colspan="5" class="text-center">
                            <ul class="pagination">
                            </ul>
                        </td>
                    </tr>
                    </tfoot>
                </table>
            </div>

            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <h3 class="text-center">Группы пользователя</h3>
                <div class="box-body">
                    Search: <input id="filter" type="text" class="form-control input-sm w-auto inline m-r"/>
                    <a class="btn btn-icon white" href="/admin/users/{{ $user->id }}/groups/add">
                        <i class="fa fa-plus"></i>
                    </a>
                </div>

                <table class="table m-b-none" data-ui-jp="footable" data-filter="#filter" data-page-size="5">
                    <thead>
                    <tr>
                        <th data-toggle="true">
                            ID
                        </th>
                        <th>
                            Название
                        </th>
                        <th>
                            Управление
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($user->groups as $group)
                        <tr>
                            <td>{{ $group->id }}</td>
                            <td>
                                <a href="/admin/users/{{ $user->id }}/groups/{{ $group->id }}/change">{{ $group->name }}</a>
                            </td>
                            <td class="manage">
                                <a class="btn btn-icon white"
                                   href="/admin/users/{{ $user->id }}/groups/{{ $group->id }}/change">
                                    <i class="ion-edit"></i>
                                </a>
                                <a class="btn btn-icon white"
                                   href="/admin/users/{{ $user->id }}/groups/{{ $group->id }}/delete">
                                    <i class="ion-trash-a"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                    <tfoot class="hide-if-no-paging">
                    <tr>
                        <td colspan="5" class="text-center">
                            <ul class="pagination">
                            </ul>
                        </td>
                    </tr>
                    </tfoot>
                </table>
            </div>
        @endif
    </div>
@endsection