@extends('admin.users.html')
@section('center-settings')
    @include('admin.layouts.common.title-box', ['title' => isset($role) ? 'Изменение роли ' . $role->name . ' у пользователя ' . $user->name : 'Создание новой роли у ' . $user->name, 'titleSmall' => isset($role) ? 'Роль ' . $role->name : 'Заполните форму для добавления новой роли пользователю.'])
    <div class="padding">
        <form action="/admin/users/{{ $user->id }}/roles/{{ isset($role) ? $role->id . '/change' : 'add' }}" method="POST">
            <div class="form-group row">
                <label for="single" class="col-sm-2 form-control-label">Роль</label>
                <div class="col-sm-10">
                    <select id="single" class="form-control select2" data-ui-jp="select2" name="role_id" data-ui-options="{theme: 'bootstrap'}">
                        @foreach($roles as $oneRole)
                                <option value="{{ $oneRole->id }}" {{ isset($role) ? $oneRole->id == $role->id ? 'selected' : '' : '' }}>{{ $oneRole->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            {{ csrf_field() }}
            @if(isset($role))
                <input type="hidden" name="_method" value="put">
            @endif
            <div class="form-group row m-t-lg">
                <div class="col-sm-4 col-sm-offset-2">
                    <a class="btn white" href="/admin/users/{{ $user->id }}/edit">Отмена</a>
                    <button type="submit" class="btn btn-primary">Сохранить</button>
                </div>
            </div>
        </form>
    </div>
@endsection