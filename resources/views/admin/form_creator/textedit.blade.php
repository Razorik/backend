<div class="form-group row">
    <label class="col-sm-2 form-control-label">{{ $field->title }}</label>
    <div class="col-sm-10 box m-b-md">
        <div data-ui-jp="summernote">
            {!! $entry[$field['name']] !!}
        </div>
    </div>
</div>