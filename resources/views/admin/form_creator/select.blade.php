<div class="form-group row">
    <label for="single" class="col-sm-2 form-control-label">{{ $field->title }}</label>
    <div class="col-sm-10">
        <select id="single" class="form-control select2" data-ui-jp="select2" name="{{ $field->name }}"
                data-ui-options="{theme: 'bootstrap'}">
            @if(isset($options))
                @foreach($options as $option)
                    <option value="{{ $option['value'] }}" {{ (isset($entry) && $entry[$field['name']] == $option['value']) ? 'selected' : '' }}>{{ $option['text'] }}</option>
                @endforeach
            @endif
        </select>
    </div>
</div>