<div class="form-group row">
    <label class="col-sm-2 form-control-label">{{ $field->title }}</label>
    <div class="col-sm-10">
        <textarea name="{{ $field->name }}" class="form-control" rows="5">{{ isset($entry) ? $entry[$field['name']] : '' }}</textarea>
    </div>
</div>