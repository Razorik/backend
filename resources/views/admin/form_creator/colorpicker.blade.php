<div class="form-group row">
    <label for="colorpicker" class="form-control-label col-sm-2">{{ $field->title }}</label>
    <div class="form-group col-sm-10">
        <input name="{{ $field->name }}" class="form-control" type="color" id="colorpicker" value="{{ isset($entry) ? $entry[$field['name']] : '' }}"}}>
    </div>
</div>