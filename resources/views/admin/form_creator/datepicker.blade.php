<div class="form-group row">
<label for="datepicker" class="col-sm-2 form-control-label">{{ $field->title }}</label>
<div class="form-group col-sm-10">
    <input id="datepicker" type='text' name="{{ $field->name }}" class="form-control" data-ui-jp="datetimepicker" value="{{ isset($entry) ? $entry[$field['name']] : '' }}" data-ui-options="{
            icons: {
              time: 'fa fa-clock-o',
              date: 'fa fa-calendar',
              up: 'fa fa-chevron-up',
              down: 'fa fa-chevron-down',
              previous: 'fa fa-chevron-left',
              next: 'fa fa-chevron-right',
              today: 'fa fa-screenshot',
              clear: 'fa fa-trash',
              close: 'fa fa-remove'
            }
          }">
</div>
</div>