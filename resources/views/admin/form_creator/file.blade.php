<div class="form-group row">
    <label for="filepicker" class="form-control-label col-sm-2">{{ $field->title }}</label>
    <div class="form-group col-sm-8">
        <input class="col-sm-4" type="file" name="{{ $field->name }}" class="form-control" id="filepicker-{{ $field->name }}" {{ $field->required==1 ? 'required' : '' }}>
        <img class="col-sm-2" src="{{ isset($entry) ? $entry[$field['name']] : '' }}">
    </div>
</div>