<div class="form-group row">
    <label for="multiple" class="col-sm-2 form-control-label">{{ $field->title }}</label>
    <div class="col-sm-10">
        <select id="multiple" name="{{ $field->name }}" class="form-control select2-multiple" multiple data-ui-jp="select2" data-ui-options="{theme: 'bootstrap'}">
            @if(isset($options))
                @foreach($options as $option)
                    <option value="{{ $option['value'] }}" {{ (isset($entry) && $entry[$field['name']] == $option['value']) ? 'selected' : '' }}>{{ $option['text'] }}</option>
                @endforeach
            @endif
        </select>
    </div>
</div>