<div class="form-group row">
    <label class="col-sm-2 form-control-label">{{ $field->title }}</label>
    <div class="col-sm-10">
        <input name="{{ $field->name }}" type="text" value="{{ $entry[$field['name']] }}" class="form-control" placeholder="{{ $field->placeholder }}" {{ $field->required==1 ? 'required' : '' }}>
    </div>
</div>