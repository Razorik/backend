<div class="box-header">
    <h2>{{ $title }}</h2>
    <small>{{ $titleSmall }}</small>
</div>