@extends('admin.layouts.template.app')
@section('content')
<div id="content" class="app-content box-shadow-z2 bg pjax-container" role="main">
    @yield('header')
    @yield('footer')
    <div class="app-body">
        <div class="row-col">
            <div class="box">
                @yield('center')
            </div>
            @yield('right-sidebar')
        </div>
    </div>
</div>
@endsection