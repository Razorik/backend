<div class="app-footer white bg p-a b-t">
    <div class="pull-right text-sm text-muted">Version {{ env('BACKEND_VERSION') }}</div>
    <span class="text-sm text-muted">&copy; Copyright.</span>
</div>