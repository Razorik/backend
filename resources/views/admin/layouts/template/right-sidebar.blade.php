<div class="col-lg w-lg w-auto-md white bg">
    <div>
        <div class="p-a">
            <h6 class="text-muted m-a-0">Quick chat</h6>
        </div>
        <div class="list inset">
            <a class="list-item" data-toggle="modal" data-target="#chat" data-dismiss="modal">
		            <span class="list-left">
		            	<span class="avatar">
		            		<i class="on avatar-center no-border"></i>
		                	<img src="{{ asset('backend/images/a1.jpg') }}" class="w-20" alt=".">
		                </span>
		            </span>
                <span class="list-body text-ellipsis">
		            	Jonathan Morina
		            </span>
            </a>
            <a class="list-item" data-toggle="modal" data-target="#chat" data-dismiss="modal">
		            <span class="list-left">
		            	<span class="avatar">
		            		<i class="on avatar-center no-border"></i>
		                	<img src="{{ asset('backend/images/a2.jpg') }}" class="w-20" alt=".">
		                </span>
		            </span>
                <span class="list-body text-ellipsis">
		            	Crystal Guerrero
		            </span>
            </a>
            <a class="list-item" data-toggle="modal" data-target="#chat" data-dismiss="modal">
		            <span class="list-left">
		            	<span class="avatar">
		            		<i class="on avatar-center no-border"></i>
		                	<img src="{{ asset('backend/images/a3.jpg') }}" class="w-20" alt=".">
		                </span>
		            </span>
                <span class="list-body text-ellipsis">
		            	Judith Garcia
		            </span>
            </a>
            <a class="list-item" data-toggle="modal" data-target="#chat" data-dismiss="modal">
		            <span class="list-left">
		            	<span class="avatar">
		            		<i class="away avatar-center no-border"></i>
		                	<img src="{{ asset('backend/images/a4.jpg') }}" class="w-20" alt=".">
		                </span>
		            </span>
                <span class="list-body text-ellipsis">
		            	Melissa Garza
		            </span>
            </a>
            <a class="list-item" data-toggle="modal" data-target="#chat" data-dismiss="modal">
		            <span class="list-left">
		            	<span class="avatar">
		            		<i class="off avatar-center no-border"></i>
		                	<img src="{{ asset('backend/images/a4.jpg') }}" class="w-20" alt=".">
		                </span>
		            </span>
                <span class="list-body text-ellipsis">
		            	Douglas Torres
		            </span>
            </a>
        </div>
        <div class="p-a">
            <h6 class="text-muted m-a-0">Activities</h6>
        </div>
        <div class="streamline streamline-theme m-b">
            <div class="sl-item b-success">
                <div class="sl-content">
                    <div>Finished task <a href="#" class="text-info">#features 4</a>.</div>
                    <div class="sl-date text-muted">Just now</div>
                </div>
            </div>
            <div class="sl-item b-success active">
                <div class="sl-content">
                    <div><a href="#">@Jessi</a> uploaded a file <a href="#" class="text-info">documentation.pdf</a></div>
                    <div class="sl-date text-muted">11:30</div>
                </div>
            </div>
            <div class="sl-item b-info">
                <div class="sl-content">
                    <div>Call to customer <a href="#" class="text-info">Jacob</a> and discuss the detail.</div>
                    <div class="sl-date text-muted">10:30</div>
                </div>
            </div>
            <div class="sl-item">
                <div class="sl-content">
                    <div><a href="#" class="text-info">Jessi</a> commented your post.</div>
                    <div class="sl-date text-muted">3 days ago</div>
                </div>
            </div>
            <div class="sl-item">
                <div class="sl-content">
                    <div><a href="#" class="text-info">Jessi</a> report a issue #2122.</div>
                    <div class="sl-date text-muted">Thu, 10 Mar</div>
                </div>
            </div>
            <div class="sl-item">
                <div class="sl-content">
                    <div>Prepare for presentation</div>
                    <div class="sl-date text-muted">Sat, 5 Mar</div>
                </div>
            </div>
        </div>
    </div>
</div>