<!-- aside -->
<div id="aside" class="app-aside fade nav-dropdown black">
    <!-- fluid app aside -->
    <div class="navside dk" data-layout="column">
        <div class="navbar no-radius">
            <!-- brand -->
            <a href="/admin" class="navbar-brand">
                <div data-ui-include="'{{ asset('backend/images/logo.svg') }}'"></div>
                <img src="{{ asset('backend/images/logo.png') }}" alt="." class="hide">
                <span class="hidden-folded inline">{{ env('APP_NAME') }}</span>
            </a>
            <!-- / brand -->
        </div>
        <div data-flex class="hide-scroll">
            <nav class="scroll nav-stacked nav-stacked-rounded nav-color">
                <ul class="nav" data-ui-nav>
                    <li class="nav-header hidden-folded">
                        <span class="text-xs">Модули</span>
                    </li>
                    <li>
                        <a href="/admin" class="b-danger">
                            <span class="nav-icon text-white no-fade">
                                <i class="ion-filing"></i>
                            </span>
                            <span class="nav-text">Главная страница</span>
                        </a>
                    </li>
                    @if(checkUserAccess($user_info, 950))
                    <li>
                        <a href="/admin/settings" class="b-danger">
                            <span class="nav-icon text-white no-fade">
                                <i class="fa fa-cogs"></i>
                            </span>
                            <span class="nav-text">Настройки сайта</span>
                        </a>
                    </li>
                    @endif
                    @if(checkUserAccess($user_info, 1000))
                    <li>
                        <a href="/admin/manage" class="b-danger">
                            <span class="nav-icon text-white no-fade">
                                <i class="fa fa-cogs"></i>
                            </span>
                            <span class="nav-text">Управление</span>
                        </a>
                    </li>
                    @endif
                    @if(checkUserAccess($user_info, 950))
                    <li>
                        <a href="/admin/users" class="b-danger">
                            <span class="nav-icon text-white no-fade">
                                <i class="fa fa-cogs"></i>
                            </span>
                            <span class="nav-text">Пользователи сайта</span>
                        </a>
                    </li>
                    @endif
                    @if(checkUserAccess($user_info, 950))
                    <li>
                        <a href="/admin/groups" class="b-danger">
                            <span class="nav-icon text-white no-fade">
                                <i class="fa fa-cogs"></i>
                            </span>
                            <span class="nav-text">Группы пользователей</span>
                        </a>
                    </li>
                    @endif
                    @if(checkUserAccess($user_info, 950))
                    <li>
                        <a href="/admin/roles" class="b-danger">
                            <span class="nav-icon text-white no-fade">
                                <i class="fa fa-cogs"></i>
                            </span>
                            <span class="nav-text">Роли пользователей</span>
                        </a>
                    </li>
                    @endif
                    @if(checkUserAccess($user_info, 950))
                    <li>
                        <a href="/admin/permissions" class="b-danger">
                            <span class="nav-icon text-white no-fade">
                                <i class="fa fa-cogs"></i>
                            </span>
                            <span class="nav-text">Права доступа</span>
                        </a>
                    </li>
                    @endif
                    <li class="nav-header hidden-folded">
                        <span class="text-xs">Таблицы</span>
                    </li>
                    @foreach($tables->where('show_table', 1)->sortBy('order')->all() as $table)
                        @if(checkUserAccess($user_info, $table->access_level))
                        <li>
                            <a href="/admin/{{ $table->name }}" class="b-danger">
                                <span class="nav-icon text-white no-fade">
                                    <i class="{{ $table->icon }}"></i>
                                </span>
                                <span class="nav-text">{{ $table->title }}</span>
                            </a>
                        </li>
                        @endif
                    @endforeach
                </ul>
            </nav>
        </div>
        <div data-flex-no-shrink>
            <div class="nav-fold dropup">
                <a data-toggle="dropdown">
                    <div class="pull-left">
                        <div class="inline"><span class="avatar w-40 grey">JR</span></div>
                        <img src="{{ asset('backend/images/a0.jpg') }}" alt="..." class="w-40 img-circle hide">
                    </div>
                    @if(Auth::check())
                        <div class="clear hidden-folded p-x">
                            <span class="block _500 text-muted">{{ $user_info->name }}</span>
                            {{--<div class="progress-xxs m-y-sm lt progress">--}}
                            {{--<div class="progress-bar info" style="width: 15%;">--}}
                            {{--</div>--}}
                            {{--</div>--}}
                        </div>
                    @endif
                </a>
                <div class="dropdown-menu w dropdown-menu-scale ">
                    <a class="dropdown-item" href="/admin/logout">Sign out</a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- / -->