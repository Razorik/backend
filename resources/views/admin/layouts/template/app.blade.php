<!DOCTYPE html>
<html lang="en">
<head>
    @section('head')
        @include('admin.layouts.template.head')
    @show
</head>
<body>
    <div class="app" id="app">
        @yield('aside')
        @yield('content')
    </div>
    @section('scripts')
        @include('admin.layouts.template.scripts')
    @show
</body>
</html>
