<!-- build:js scripts/app.min.js -->
<!-- jQuery -->
<script src="{{ asset('backend/libs/jquery/dist/jquery.js') }}"></script>
<!-- Bootstrap -->
<script src="{{ asset('backend/libs/tether/dist/js/tether.min.js') }}"></script>
<script src="{{ asset('backend/libs/bootstrap/dist/js/bootstrap.js') }}"></script>
<!-- core -->
<script src="{{ asset('backend/libs/jQuery-Storage-API/jquery.storageapi.min.js') }}"></script>
<script src="{{ asset('backend/libs/PACE/pace.min.js') }}"></script>
<script src="{{ asset('backend/libs/jquery-pjax/jquery.pjax.js') }}"></script>
<script src="{{ asset('backend/libs/blockUI/jquery.blockUI.js') }}"></script>
<script src="{{ asset('backend/libs/jscroll/jquery.jscroll.min.js') }}"></script>

<script src="{{ asset('backend/scripts/config.lazyload.js') }}"></script>
<script src="{{ asset('backend/scripts/ui-load.js') }}"></script>
<script src="{{ asset('backend/scripts/ui-jp.js') }}"></script>
<script src="{{ asset('backend/scripts/ui-include.js') }}"></script>
<script src="{{ asset('backend/scripts/ui-device.js') }}"></script>
<script src="{{ asset('backend/scripts/ui-form.js') }}"></script>
<script src="{{ asset('backend/scripts/ui-modal.js') }}"></script>
<script src="{{ asset('backend/scripts/ui-nav.js') }}"></script>
<script src="{{ asset('backend/scripts/ui-list.js') }}"></script>
<script src="{{ asset('backend/scripts/ui-screenfull.js') }}"></script>
<script src="{{ asset('backend/scripts/ui-scroll-to.js') }}"></script>
<script src="{{ asset('backend/scripts/ui-toggle-class.js') }}"></script>
<script src="{{ asset('backend/scripts/ui-taburl.js') }}"></script>
<script src="{{ asset('backend/scripts/app.js') }}"></script>
<script src="{{ asset('backend/scripts/ajax.js') }}"></script>
<!-- endbuild -->