@extends('admin.modules.html')
@section('center-settings')
    @include('admin.layouts.common.title-box', ['title' => isset($entry) ? 'Изменение записи ' . $entry->name : 'Создание нового поля', 'titleSmall' => isset($entry) ? 'Запись ' . $entry->name . ' в модуле ' . $table->name  : 'Заполните форму для добавления новой записи.'])
    <div class="padding">
        <a class="btn white back-btn mr-10" href="{{ session()->get('linksToBack')[count(session()->get('linksToBack'))-2] }}">
            Вернуться <i class="fa fa-level-up"></i>
        </a>
        @if(isset($parents) && (count($parents) > 0) && isset($entry))
        <nav class="nav nav-pills children-tables">
            @foreach($parents as $parent)
                @if(checkUserAccess($user_info, $parent->access_level))
                    <a class="nav-item nav-link parents-btn" href="/admin/{{ $parent->name }}/{{ $entry->id }}">{{ $parent->title }}</a>
                @endif
            @endforeach
        </nav>
        @endif
    </div>
    <div class="padding">
        <form id="{{ $table->name }}" action="/admin/{{ $table->name }}{{ isset($entry) ? '/' . $entry->id : '' }}" method="POST" enctype="multipart/form-data">
            {!! $formContent !!}
            {{ csrf_field() }}
            @if(isset($entry))
                <input type="hidden" name="_method" value="put">
            @endif
            <div class="form-group row m-t-lg">
                <div class="col-sm-4 col-sm-offset-2">
                    <a class="btn white" href="{{ session()->get('linksToBack')[count(session()->get('linksToBack'))-2] }}">Отмена</a>
                    <button type="submit" class="btn btn-primary">Сохранить</button>
                </div>
            </div>
        </form>
    </div>
@endsection