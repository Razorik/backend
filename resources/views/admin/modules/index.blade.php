@extends('admin.modules.html')
@section('center-settings')
    @include('admin.layouts.common.title-box')
    <div class="box-body">
        @if(isset(session()->get('linksToBack')[count(session()->get('linksToBack'))-2]) && \Illuminate\Support\Facades\URL::current() != session()->get('linksToBack')[count(session()->get('linksToBack'))-2])
            <a class="btn white" href="{{ session()->get('linksToBack')[count(session()->get('linksToBack'))-2] }}">
                Вернуться <i class="fa fa-level-up"></i>
            </a>
        @endif
        Search: <input id="filter" type="text" class="form-control input-sm w-auto inline m-r"/>
        <a class="btn btn-icon white" href="/admin/{{ $table->name }}/create">
            <i class="fa fa-plus"></i>
        </a>
    </div>
    <div>
        <table class="table m-b-none" data-ui-jp="footable" data-filter="#filter" data-page-size="5">
            <thead>
            <tr>
                @foreach($table->fields->where('show_in_table', 1)->sortBy('order')->all() as $field)
                    <th>
                        {{ $field->title }}
                    </th>
                @endforeach
                <th>
                    Управление
                </th>
            </tr>
            </thead>
            <tbody>
            @foreach($entries as $entry)
                <tr>
                @foreach($table->fields->where('show_in_table', 1)->sortBy('order')->all() as $field)
                    <td>
                            {{ $entry[$field['name']] }}
                    </td>
                @endforeach
                <td class="manage">
                    <a class="btn btn-icon white" href="/admin/{{ $table->name }}/{{ $entry->id }}/edit">
                        <i class="ion-edit"></i>
                    </a>
                    <form action="/admin/{{ $table->name }}/{{ $entry->id }}" method="POST">
                        <input type="hidden" name="_method" value="delete">
                        {{ csrf_field() }}
                        <button type="submit" class="btn btn-icon white">
                            <i class="ion-trash-a"></i>
                        </button>
                    </form>
                </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection