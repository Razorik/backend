@extends('admin.manage.html')
@section('center-settings')
    @include('admin.layouts.common.title-box')
    <div class="box-body">
        Search: <input id="filter" type="text" class="form-control input-sm w-auto inline m-r"/>
        <a class="btn btn-icon white" href="/admin/manage/create">
            <i class="fa fa-plus"></i>
        </a>
    </div>
    <div>
        <table class="table m-b-none" data-ui-jp="footable" data-filter="#filter" data-page-size="5">
            <thead>
            <tr>
                <th data-toggle="true">
                    ID
                </th>
                <th>
                    Заголовок
                </th>
                <th>
                    Статус
                </th>
                <th>
                    Управление
                </th>
            </tr>
            </thead>
            <tbody>
            @foreach($tables->sortBy('order') as $table)
                <tr>
                    <td>{{ $table->id }}</td>
                    <td><a href="/admin/manage/{{ $table->id }}/edit">{{ $table->title }}</a></td>
                    <td>
                        <span class="label {{ $table->show_table=="1" ? 'success' : 'danger' }}" title="{{ $table->show_table=="1" ? 'Отображается в меню сайта' : 'Не отображается в меню сайта' }}">{{ $table->show_table=="1" ? 'Отображается' : 'Не отображается' }}</span>
                    </td>
                    <td class="manage">
                        <a class="btn btn-icon white" href="/admin/manage/{{ $table->id }}/edit">
                            <i class="ion-edit"></i>
                        </a>
                        <form action="/admin/manage/{{ $table->id }}" method="POST">
                            <input type="hidden" name="_method" value="delete">
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-icon white">
                                <i class="ion-trash-a"></i>
                            </button>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
            <tfoot class="hide-if-no-paging">
            <tr>
                <td colspan="5" class="text-center">
                    <ul class="pagination">
                    </ul>
                </td>
            </tr>
            </tfoot>
        </table>
    </div>
@endsection