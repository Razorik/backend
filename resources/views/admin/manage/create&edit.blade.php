@extends('admin.manage.html')
@section('center-settings')
    @include('admin.layouts.common.title-box', ['title' => isset($table) ? 'Изменение таблицы ' . $table->title : 'Создание новой таблицы', 'titleSmall' => isset($table) ? 'Таблица ' . $table->title : 'Заполните форму для добавления новой таблицы.'])
    <div class="padding">
        <form action="/admin/manage{{ isset($table) ? '/' . $table->id : '' }}" method="POST">
            <div class="form-group row">
                <label class="col-sm-2 form-control-label">Заголовок таблицы</label>
                <div class="col-sm-10">
                    <input name="title" type="text" value="{{ isset($table) ? $table->title : '' }}" class="form-control" placeholder="Введите заголовок таблицы" required>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 form-control-label">Название таблицы (латиницей)</label>
                <div class="col-sm-10">
                    <input name="name" type="text" value="{{ isset($table) ? $table->name : '' }}" class="form-control" placeholder="Введите название таблицы латиницей" required>
                </div>
            </div>
            <div class="form-group row">
                <label for="single" class="col-sm-2 form-control-label">Родительская таблица</label>
                <div class="col-sm-10">
                    <select id="single" class="form-control select2" data-ui-jp="select2" name="parent_id" data-ui-options="{theme: 'bootstrap'}">
                        <option value="" {{ isset($table) ? isset($table->parent_id) ? '' : 'checked' : 'checked' }}></option>
                        @foreach($tables->sortBy('order') as $oneTable)
                            @if(!isset($table) || $oneTable->id != $table->id)
                                <option value="{{ $oneTable->id }}" {{ isset($table) ? isset($table->parent_id) ? $oneTable->id == $table->parent_id ? 'selected' : '' : '' : '' }}>{{ $oneTable->name }}</option>
                            @endif
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 form-control-label">Описание</label>
                <div class="col-sm-10">
                    <textarea name="description" class="form-control" rows="5">{{ isset($table) ? $table->description : '' }}</textarea>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 form-control-label">Иконка</label>
                <div class="col-sm-10">
                    <input name="icon" type="text" value="{{ isset($table) ? $table->icon : '' }}" class="form-control" placeholder="Укажите класс иконки">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 form-control-label">Пользовательский контроллер</label>
                <div class="col-sm-10">
                    <input name="custom_controller" type="text" value="{{ isset($table) ? $table->custom_controller : '' }}" class="form-control" placeholder="Введите название контроллера которым нужно обрабатывать эту таблицу">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 form-control-label">Количество отображаемых полей на одной странице</label>
                <div class="col-sm-10">
                    <input name="count_fields" type="text" value="{{ isset($table) ? $table->count_fields : '' }}" class="form-control" placeholder="Введите количество полей, которые будут отображаться на одной странице">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 form-control-label">Сортировка</label>
                <div class="col-sm-10">
                    <input name="order" type="text" value="{{ isset($table) ? $table->order : '' }}" class="form-control" placeholder="Введите число с помощью которого таблицы будут сортироватся" required>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 form-control-label">Уровень доступа</label>
                <div class="col-sm-10">
                    <input name="access_level" type="text" value="{{ isset($table) ? $table->access_level : '' }}" class="form-control" placeholder="Введите уровень доступа к таблице (целое число)" required>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 form-control-label">Показывать таблицу в меню?</label>
                <div class="col-sm-10">
                    <label class="ui-switch m-t-xs m-r">
                        <input name="show_table" type="checkbox" {{ isset($table) ? $table->show_table ==  '1' ? 'checked' : '' : ''  }} class="has-value">
                        <i></i>
                    </label>
                </div>
            </div>
            {{ csrf_field() }}
            @if(isset($table))
                <input type="hidden" name="_method" value="put">
            @endif
            <div class="form-group row m-t-lg">
                <div class="col-sm-4 col-sm-offset-2">
                    <a class="btn white" href="/admin/manage">Отмена</a>
                    <button type="submit" class="btn btn-primary">Сохранить</button>
                </div>
            </div>
        </form>
        @if(isset($table))
        <h3 class="text-center">Поля таблицы</h3>
        <div class="box-body">
            Search: <input id="filter" type="text" class="form-control input-sm w-auto inline m-r"/>
            <a class="btn btn-icon white" href="/admin/manage/{{ $table->id }}/fields/create">
                <i class="fa fa-plus"></i>
            </a>
        </div>

        <table class="table m-b-none" data-ui-jp="footable" data-filter="#filter" data-page-size="{{ $table->count_fields }}">
            <thead>
            <tr>
                <th data-toggle="true">
                    ID
                </th>
                <th>
                    Заголовок
                </th>
                <th>
                    Тип поля
                </th>
                <th>
                    Тип данных
                </th>
                <th>
                    Показывать в таблице
                </th>
                <th>
                    Показывать в форме
                </th>
                <th>
                    Управление
                </th>
            </tr>
            </thead>
            <tbody>
            @foreach($table->fields->sortBy('order') as $field)
                <tr>
                    <td>{{ $field->id }}</td>
                    <td>
                        <a href="/admin/manage/{{ $table->id }}/fields/{{ $field->id }}/edit">{{ $field->title }}</a>
                    </td>
                    <td>{{ $field->f_type }}</td>
                    <td>{{ $field->d_type }}</td>
                    <td>
                        <span class="label {{ $field->show_in_table=="1" ? 'success' : 'danger' }}" title="{{ $field->show_in_table=="1" ? 'Отображается в заголовке таблице' : 'Не отображается в заголовке таблицы' }}">{{ $field->show_in_table=="1" ? 'Да' : 'Нет' }}</span>
                    </td>
                    <td>
                        <span class="label {{ $field->show_in_form=="1" ? 'success' : 'danger' }}" title="{{ $field->show_in_form=="1" ? 'Отображается в форме' : 'Не отображается в форме' }}">{{ $field->show_in_form=="1" ? 'Да' : 'Нет' }}</span>
                    </td>
                    <td class="manage">
                        <a class="btn btn-icon white" href="/admin/manage/{{ $table->id }}/fields/{{ $field->id }}/edit">
                            <i class="ion-edit"></i>
                        </a>
                        <form action="/admin/manage/{{ $table->id }}/fields/{{ $field->id }}" method="POST">
                            <input type="hidden" name="_method" value="delete">
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-icon white">
                                <i class="ion-trash-a"></i>
                            </button>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
            <tfoot class="hide-if-no-paging">
            <tr>
                <td colspan="5" class="text-center">
                    <ul class="pagination">
                    </ul>
                </td>
            </tr>
            </tfoot>
        </table>
        @endif
    </div>
@endsection