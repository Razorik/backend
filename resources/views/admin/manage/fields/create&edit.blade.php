@extends('admin.manage.fields.html')
@section('center-settings')
    @include('admin.layouts.common.title-box', ['title' => isset($field) ? 'Изменение поля ' . $field->title : 'Создание нового поля', 'titleSmall' => isset($field) ? 'Поле ' . $field->title . ' в таблице ' . $field->table->title  : 'Заполните форму для добавления нового поля.'])
    <div class="padding">
        <form action="/admin/manage{{ isset($field) ? '/' . $field->table->id : '/' . $table->id }}/fields{{ isset($field) ? '/' . $field->id : '' }}" method="POST">
            <div class="form-group row">
                <label class="col-sm-2 form-control-label">Заголовок поля</label>
                <div class="col-sm-10">
                    <input name="title" type="text" value="{{ isset($field) ? $field->title : '' }}"
                           class="form-control" placeholder="Введите заголовок поля" required>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 form-control-label">Название поля (латиницей)</label>
                <div class="col-sm-10">
                    <input name="name" type="text" value="{{ isset($field) ? $field->name : '' }}" class="form-control"
                           placeholder="Введите название поля латиницей" required>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-10">
                    <input name="parent_id" type="hidden" value="{{ isset($field) ? $field->parent_id : (isset($table) ? $table->id : '') }}" class="form-control"
                           placeholder="Введите id родительского элемента" required>
                </div>
            </div>
            <div class="form-group row">
                <label for="single" class="col-sm-2 form-control-label">Тип поля</label>
                <div class="col-sm-10">
                    <select id="single" class="form-control select2" data-ui-jp="select2" name="f_type" data-ui-options="{theme: 'bootstrap'}">
                        <option value="text" {{ isset($field) ? isset($field->f_type) ? $field->f_type == 'text' ? 'selected' : '' : '' : '' }}>Текстовое поле (text)</option>
                        <option value="select" {{ isset($field) ? isset($field->f_type) ? $field->f_type == 'select' ? 'selected' : '' : '' : '' }}>Выпадающий список (select)</option>
                        <option value="textarea" {{ isset($field) ? isset($field->f_type) ? $field->f_type == 'textarea' ? 'selected' : '' : '' : '' }}>Большое текстовое поле (textarea)</option>
                        <option value="textedit" {{ isset($field) ? isset($field->f_type) ? $field->f_type == 'textedit' ? 'selected' : '' : '' : '' }}>Текстовый редактор (textedit)</option>
                        <option value="slug" {{ isset($field) ? isset($field->f_type) ? $field->f_type == 'slug' ? 'selected' : '' : '' : '' }}>Слаг (slug)</option>
                        <option value="multiselect" {{ isset($field) ? isset($field->f_type) ? $field->f_type == 'multiselect' ? 'selected' : '' : '' : '' }}>Множественный выпадающий список (multiselect)</option>
                        <option value="datepicker" {{ isset($field) ? isset($field->f_type) ? $field->f_type == 'datepicker' ? 'selected' : '' : '' : '' }}>Поле выбора даты и времени (datepicker)</option>
                        <option value="colorpicker" {{ isset($field) ? isset($field->f_type) ? $field->f_type == 'colorpicker' ? 'selected' : '' : '' : '' }}>Поле выбора цвета (colorpicker)</option>
                        {{--<option value="image" {{ isset($field) ? isset($field->f_type) ? $field->f_type == 'image' ? 'selected' : '' : '' : '' }}>Изображение (image)</option>--}}
                        <option value="file" {{ isset($field) ? isset($field->f_type) ? $field->f_type == 'file' ? 'selected' : '' : '' : '' }}>Файл (file)</option>
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label for="single" class="col-sm-2 form-control-label">Тип данных</label>
                <div class="col-sm-10">
                    <select id="single" class="form-control select2" data-ui-jp="select2" name="d_type" data-ui-options="{theme: 'bootstrap'}">
                        <option value="int" {{ isset($field) ? isset($field->d_type) ? $field->d_type == 'int' ? 'selected' : '' : '' : '' }}>Целое число (int)</option>
                        <option value="float" {{ isset($field) ? isset($field->d_type) ? $field->d_type == 'float' ? 'selected' : '' : '' : '' }}>Число с плавающей точкой (float)</option>
                        <option value="varchar" {{ isset($field) ? isset($field->d_type) ? $field->d_type == 'varchar' ? 'selected' : '' : '' : '' }}>Строка (varchar)</option>
                        <option value="text" {{ isset($field) ? isset($field->d_type) ? $field->d_type == 'text' ? 'selected' : '' : '' : '' }}>Текст (text)</option>
                        <option value="datetime" {{ isset($field) ? isset($field->d_type) ? $field->d_type == 'datetime' ? 'selected' : '' : '' : '' }}>Дата и время (datetime)</option>
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 form-control-label">Подсказка</label>
                <div class="col-sm-10">
                    <input name="placeholder" type="text" value="{{ isset($field) ? $field->placeholder : '' }}" class="form-control"
                           placeholder="Введите подсказку" required>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 form-control-label">Значение по умолчанию</label>
                <div class="col-sm-10">
                    <input name="default" type="text" value="{{ isset($field) ? $field->default : '' }}" class="form-control"
                           placeholder="Введите значение по умолчанию">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 form-control-label">Описание</label>
                <div class="col-sm-10">
                    <textarea name="description" class="form-control" rows="5">{{ isset($field) ? $field->description : '' }}</textarea>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 form-control-label">Настройки</label>
                <div class="col-sm-10">
                    <textarea name="options" class="form-control" rows="5">{{ isset($field) ? $field->options : '' }}</textarea>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 form-control-label">Сортировка</label>
                <div class="col-sm-10">
                    <input name="order" type="text" value="{{ isset($field) ? $field->order : '' }}" class="form-control" placeholder="Введите число с помощью которого поля будут сортироватся" required>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 form-control-label">Может быть NULL?</label>
                <div class="col-sm-10">
                    <label class="ui-switch m-t-xs m-r">
                        <input name="nullable" type="checkbox" {{ isset($field) ? $field->nullable ==  '1' ? 'checked' : '' : ''  }} class="has-value">
                        <i></i>
                    </label>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 form-control-label">Обязательное поле?</label>
                <div class="col-sm-10">
                    <label class="ui-switch m-t-xs m-r">
                        <input name="required" type="checkbox" {{ isset($field) ? $field->required ==  '1' ? 'checked' : '' : ''  }} class="has-value">
                        <i></i>
                    </label>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 form-control-label">Показывать поле в заголовке таблицы?</label>
                <div class="col-sm-10">
                    <label class="ui-switch m-t-xs m-r">
                        <input name="show_in_table" type="checkbox" {{ isset($field) ? $field->show_in_table ==  '1' ? 'checked' : '' : ''  }} class="has-value">
                        <i></i>
                    </label>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 form-control-label">Показывать поле в форме?</label>
                <div class="col-sm-10">
                    <label class="ui-switch m-t-xs m-r">
                        <input name="show_in_form" type="checkbox" {{ isset($field) ? $field->show_in_form ==  '1' ? 'checked' : '' : ''  }} class="has-value">
                        <i></i>
                    </label>
                </div>
            </div>
            {{ csrf_field() }}
            @if(isset($field))
                <input type="hidden" name="_method" value="put">
            @endif
            <div class="form-group row m-t-lg">
                <div class="col-sm-4 col-sm-offset-2">
                    <a class="btn white" href="/admin/manage/{{ isset($field) ? $field->table->id : $table->id }}/edit">Отмена</a>
                    <button type="submit" class="btn btn-primary">Сохранить</button>
                </div>
            </div>
        </form>
    </div>
@endsection