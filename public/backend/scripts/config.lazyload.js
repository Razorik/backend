// lazyload config
var MODULE_CONFIG = {
    screenfull:     [
                      '/backend/libs/screenfull/dist/screenfull.min.js'
                    ],
    stick_in_parent:[
                      '/backend/libs/sticky-kit/jquery.sticky-kit.min.js'
                    ],
    owlCarousel:    [
            				  '/backend/libs/owl.carousel/dist/assets/owl.carousel.min.css',
            				  '/backend/libs/owl.carousel/dist/assets/owl.theme.css',
            				  '/backend/libs/owl.carousel/dist/owl.carousel.min.js'
                    ],
    easyPieChart:   [ '/backend/libs/jquery.easy-pie-chart/dist/jquery.easypiechart.fill.js' ],
    sparkline:      [ '/backend/libs/jquery.sparkline/dist/jquery.sparkline.retina.js' ],
    plot:           [ '/backend/libs/flot/jquery.flot.js',
                      '/backend/libs/flot/jquery.flot.resize.js',
                      '/backend/libs/flot/jquery.flot.pie.js',
                      '/backend/libs/flot.tooltip/js/jquery.flot.tooltip.min.js',
                      '/backend/libs/flot-spline/js/jquery.flot.spline.min.js',
                      '/backend/libs/flot.orderbars/js/jquery.flot.orderBars.js'
                    ],
    echarts:        [
                      '/backend/libs/echarts/build/dist/echarts-all.js',
                      '/backend/libs/echarts/build/dist/theme.js',
                      '/backend/libs/echarts/build/dist/jquery.echarts.js'
                    ],
    chart:          [
                      '/backend/libs/chart.js/dist/Chart.bundle.min.js',
                      '/backend/libs/chart.js/dist/jquery.chart.js'
                    ],
    vectorMap:      [ '/backend/libs/bower-jvectormap/jquery-jvectormap-1.2.2.min.js',
                      '/backend/libs/bower-jvectormap/jquery-jvectormap.css',
                      '/backend/libs/bower-jvectormap/jquery-jvectormap-world-mill-en.js',
                      '/backend/libs/bower-jvectormap/jquery-jvectormap-us-aea-en.js'
                    ],
    dataTable:      [
                      '/backend/libs/datatables/media/js/jquery.dataTables.min.js',
                      '/backend/libs/datatables/media/js/dataTables.bootstrap4.min.js',
                      '/backend/libs/datatables/media/css/dataTables.bootstrap4.min.css',
                      '/backend/libs/datatables/extensions/buttons/dataTables.buttons.min.js',
                      '/backend/libs/datatables/extensions/buttons/buttons.bootstrap4.min.js',
                      '/backend/libs/datatables/extensions/buttons/jszip.min.js',
                      '/backend/libs/datatables/extensions/buttons/pdfmake.min.js',
                      '/backend/libs/datatables/extensions/buttons/vfs_fonts.js',
                      '/backend/libs/datatables/extensions/buttons/buttons.html5.min.js',
                      '/backend/libs/datatables/extensions/buttons/buttons.print.min.js',
                      '/backend/libs/datatables/extensions/buttons/buttons.colVis.min.js'
                    ],
    footable:       [
                      '/backend/libs/footable/dist/footable.all.min.js',
                      '/backend/libs/footable/css/footable.core.css'
                    ],
    sortable:       [
                      '/backend/libs/html.sortable/dist/html.sortable.min.js'
                    ],
    nestable:       [
                      '/backend/libs/nestable/jquery.nestable.css',
                      '/backend/libs/nestable/jquery.nestable.js'
                    ],
    summernote:     [
                      '/backend/libs/summernote/dist/summernote.css',
                      '/backend/libs/summernote/dist/summernote.js'
                    ],
    parsley:        [
                      '/backend/libs/parsleyjs/dist/parsley.css',
                      '/backend/libs/parsleyjs/dist/parsley.min.js'
                    ],
    select2:        [
                      '/backend/libs/select2/dist/css/select2.min.css',
                      '/backend/libs/select2-bootstrap-theme/dist/select2-bootstrap.min.css',
                      '/backend/libs/select2-bootstrap-theme/dist/select2-bootstrap.4.css',
                      '/backend/libs/select2/dist/js/select2.min.js'
                    ],
    datetimepicker: [
                      '/backend/libs/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css',
                      '/backend/libs/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.dark.css',
                      '/backend/libs/moment/moment.js',
                      '/backend/libs/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js'
                    ],
    bootstrapWizard:[
                      '/backend/libs/twitter-bootstrap-wizard/jquery.bootstrap.wizard.min.js'
                    ],
    fullCalendar:   [
                      '/backend/libs/moment/moment.js',
                      '/backend/libs/fullcalendar/dist/fullcalendar.min.js',
                      '/backend/libs/fullcalendar/dist/fullcalendar.css',
                      '/backend/libs/fullcalendar/dist/fullcalendar.theme.css',
                      '/backend/scripts/plugins/calendar.js'
                    ],
    dropzone:       [
                      '/backend/libs/dropzone/dist/min/dropzone.min.js',
                      '/backend/libs/dropzone/dist/min/dropzone.min.css'
                    ]
  };
  
// jQuery plugin default options
var JP_CONFIG = {};
