<?php

namespace App\Http\Helpers;

use App\Field;
use Illuminate\Support\Facades\DB;

class TableEditor
{

    private $query;
    private $field;

    public function __construct(Field $field)
    {
        $this->field = $field;
    }

    public function alterTable()
    {
        $this->query .= 'ALTER TABLE ' . $this->field->table->name . ' ';
        return $this;
    }

    public function addField() {
        $this->query .= 'ADD COLUMN `' . $this->field->name . '` ' . $this->getTypeField($this->field) . ';';
        return $this;
    }

    public function editField()
    {
        $this->query .= 'MODIFY COLUMN `' . $this->field->name . '` ' . $this->getTypeField($this->field) . ';';
        return $this;
    }

    public function deleteField()
    {
        $this->query .= 'DROP COLUMN `' . $this->field->name . '`;';
        return $this;
    }

    public function run() {
        DB::statement($this->query);
    }

    public function getTypeField(Field $field)
    {
        switch ($field->d_type) {
            case 'int': {
                return 'int';
                break;
            }
            case 'float': {
                return 'float';
                break;
            }
            case 'varchar': {
                return 'varchar(255)';
                break;
            }
            case 'text': {
                return 'text';
                break;
            }
            case 'datetime': {
                return 'datetime';
                break;
            }
        }
    }

}