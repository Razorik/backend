<?php

function checkUserAccess(\App\User $user, $accessLevel) {
    $can = false;
    foreach($user->roles as $role) {
        if(count($role->permissions->where('value', '>=', $accessLevel)->all()) > 0) {
            $can = true;
            break;
        }
    }
    if(!$can) {
        foreach($user->groups as $group) {
            if(count($group->permissions->where('value', '>=', $accessLevel)->all()) > 0) {
                $can = true;
                break;
            }
        }
    }
    return $can;
}

function getEntityAccess(\Illuminate\Database\Eloquent\Model $entity) {
    $permission = $entity->permissions()->orderBy('value', 'desc')->first();
    if(isset($permission)) {
        return $permission->value;
    } else {
        return 0;
    }
}

function getUserAccess(\App\User $user) {
    $groupAccess = 0;
    foreach($user->groups as $group) {
        $tmpAccess = getEntityAccess($group);
        if($groupAccess < $tmpAccess) {
            $groupAccess = $tmpAccess;
        }
    }
    $roleAccess = 0;
    foreach($user->roles as $role) {
        $tmpAccess = getEntityAccess($role);
        if($roleAccess < $tmpAccess) {
            $roleAccess = $tmpAccess;
        }
    }
    if($roleAccess > $groupAccess) {
        return $roleAccess;
    } else {
        return $groupAccess;
    }
}

?>