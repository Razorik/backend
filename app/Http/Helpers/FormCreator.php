<?php

namespace App\Http\Helpers;

use App\Field;
use App\Module;

class FormCreator
{

    protected $viewDir = 'admin.form_creator';

    public function create(Field $field, Module $entry = null)
    {
        if (method_exists($this, $field->f_type)) {
            $func = $field->f_type;
            return $this->$func($field, $entry);
        } else {
            return '';
        }
    }

    private function renderView(Field $field, $vars = []) {
        return view($this->viewDir . '.' . $field->f_type, $vars)->render();
    }

    private function text(Field $field, Module $entry = null)
    {
        return $this->renderView($field, ['entry' => $entry, 'field' => $field]);
    }

    private function select(Field $field, Module $entry = null)
    {
        if (isset($field) && isset($field->options) && strlen($field->options) > 0) {
            $vars = [];
            $vars['entry'] = $entry;
            $vars['field'] = $field;
            $options = json_decode($field->options);
            if (isset($options->list)) {
                foreach ($options->list as $key => $option) {
                    $vars['options'][] = [
                        'value' => $key,
                        'text' => $option
                    ];
                }
            } else if (isset($options->table)) {
                $table = $options->table;
                $dependsModule = new Module();
                $dependsModule->setModule($table->name);
                $options = $dependsModule->get([$table->value, $table->text])->toArray();
                foreach($options as $option) {
                    $vars['options'][] = [
                        'value' => $option[$table->value],
                        'text' => $option[$table->text]
                    ];
                }
            }
            return $this->renderView($field, $vars);
        } else {
            return '';
        }
    }

    private function textarea(Field $field , Module $entry = null) {
        return $this->renderView($field , ['entry' => $entry, 'field' => $field]);
    }

    private function textedit(Field $field, Module $entry = null) {
        return $this->renderView($field, ['entry' => $entry, 'field' => $field]);
    }

    private function slug(Field $field, Module $entry = null)
    {
        return $this->renderView($field, ['entry' => $entry, 'field' => $field]);
    }

    private function multiselect(Field $field, Module $entry = null) {
        return $this->select($field, $entry);
    }

    private function datepicker(Field $field, Module $entry = null) {
        return $this->renderView($field, ['entry' => $entry, 'field' => $field]);
    }

    private function colorpicker(Field $field, Module $entry = null) {
        return $this->renderView($field, ['entry' => $entry, 'field' => $field]);
    }

    private function file(Field $field, Module $entry = null) {
        return $this->renderView($field, ['entry' => $entry, 'field' => $field]);
    }

}