<?php
/**
 * Created by PhpStorm.
 * User: Ivan
 * Date: 05.10.2017
 * Time: 23:00
 */

namespace App\Http\Helpers;


use App\Field;
use App\Module;
use App\Table;
use Cviebrock\EloquentSluggable\Services\SlugService;

class FieldSaver
{

    public function saveField(Table $table, Field $field, $value) {
        $fieldType = $field->f_type;
        if (method_exists($this, $fieldType)) {
            if(method_exists($this, $table->name . '_' . $fieldType)) {
                $tableFieldType = $table->name . '_' . $fieldType;
                return $this->$tableFieldType($table, $field, $value);
            } else {
                return $this->$fieldType($table, $field, $value);
            }
        } else {
            return $value;
        }
    }

    private function slug(Table $table, Field $field, $value) {
        $module = new Module();
        if(strlen($value) > 0) {
            $sluggable = [
                $field->name => [
                    'source' => $field->name
                ]
            ];
            $module->sluggable = $sluggable;
            $module->setModule($table->name);
            $fieldName = $field->name;
            return SlugService::createSlug($module, $fieldName, $value);
        } else {
            return null;
        }
    }

    private function file(Table $table, Field $field, $value) {
        $origName = $value->getClientOriginalName();
        $value->storeAs('/uploads', $origName);
        return '/uploads/' . $origName;
    }

}