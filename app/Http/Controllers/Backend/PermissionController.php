<?php

namespace App\Http\Controllers\Backend;

use App\Permission;
use App\Table;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PermissionController extends HtmlController
{

    protected $tables = [
        'groups' => 'Группы',
        'roles' => 'Роли'
    ];

    public function __construct()
    {
        parent::__construct();
        $this->vars['title'] = 'Управление правами доступа сайта';
        $this->vars['titleSmall'] = 'Добавьте здесь права доступа, а затем прикрепите их к роли или группе';
    }

    public function index()
    {
        $this->vars['permissions'] = Permission::where('value','<=', getUserAccess($this->vars['user_info']))->get();
        return view('admin.permissions.index', $this->vars);
    }

    public function create()
    {
        return view('admin.permissions.create&edit', $this->vars);
    }

    public function store(Request $request)
    {
        $fields = $request->all();
        $fields = $this->prepareData($fields);
        $permission = new Permission();
        $this->save($permission, $fields, true);
        return redirect()->route('permissions.index');
    }

    public function edit($id)
    {
        $this->vars['permission'] = Permission::find($id);
        if(!checkUserAccess($this->vars['user_info'], $this->vars['permission']->value)) {
            return redirect()->back();
        }
        return view('admin.permissions.create&edit', $this->vars);
    }

    public function update(Request $request, $id)
    {
        if($request->value > getUserAccess($this->vars['user_info'])) {
            return redirect()->route('permissions.index');
        }
        $fields = $request->all();
        $fields = $this->prepareData($fields);
        $permission = Permission::find($id);
        $this->save($permission, $fields, false);
        return redirect()->route('permissions.index');
    }

    public function destroy($id)
    {
        $permission = Permission::find($id);
        $permission->roles()->detach();
        $permission->groups()->detach();
        $permission->delete();
        return redirect()->route('permissions.index');
    }

    public function addPermissionForm($entityType, $entityId) {
        $this->getEntityData($entityType, $entityId);
        $this->vars['permissions'] = Permission::all();
        $this->vars['permissions'] = $this->preparePermissions($this->vars['entity'], $this->vars['permissions']);
        return view('admin.permissions.permissionAdd&Change', $this->vars);
    }

    public function addPermission($entityType, $entityId, Request $request) {
        $this->getEntityData($entityType, $entityId);
        $this->vars['entity']->permissions()->attach($request->permission_id);
        return redirect()->route($entityType.'.edit', [$entityId]);
    }

    public function editPermissionForm($entityType, $entityId, $permissionId) {
        $this->getEntityData($entityType, $entityId);
        $this->vars['permissions'] = Permission::all();
        $this->vars['permission'] = $this->vars['permissions']->where('id', $permissionId)->first();
        $this->vars['permissions'] = $this->preparePermissions($this->vars['entity'], $this->vars['permissions']);
        return view('admin.permissions.permissionAdd&Change', $this->vars);
    }

    public function editPermission($entityType, $entityId, $permissionId, Request $request) {
        $this->getEntityData($entityType, $entityId);
        $this->vars['permission'] = Permission::find($permissionId);
        $this->vars['entity']->permissions()->updateExistingPivot($permissionId, [
            'permission_id' => $request->permission_id
        ]);
        return redirect()->route($entityType.'.edit', [$entityId]);
    }

    public function deletePermission($entityType, $entityId, $permissionId) {
        $this->getEntityData($entityType, $entityId);
        $this->vars['entity']->permissions()->detach($permissionId);
        return redirect()->route($entityType.'.edit', [$entityId]);
    }

    protected function preparePermissions(Model $entity, $permissions) {
        if(isset($entity->permissions)) {
            foreach($entity->permissions as $entityPermission) {
                foreach($permissions as $id=>$permission) {
                    if($permission->id == $entityPermission->id) {
                        unset($permissions[$id]);
                    }
                }
            }
        }
        $permissions = $this->deleteMajorPermission($entity, $permissions);
        return $permissions;
    }

    protected function deleteMajorPermission(Model $entity, $permissions) {
        if(isset($entity->permissions)) {
            foreach ($permissions as $id => $permission) {
                if (!checkUserAccess($this->vars['user_info'], $permission->value)) {
                    unset($permissions[$id]);
                }
            }
        }
        return $permissions;
    }

    protected function getEntityData($entityType, $entityId) {
        $class = 'App\\' . ucfirst(substr($entityType, 0, -1));
        $this->vars['entity'] = $class::find($entityId);
        $this->vars['entityName'] = $this->tables[$entityType];
        $this->vars['entityType'] = $entityType;
    }
}
