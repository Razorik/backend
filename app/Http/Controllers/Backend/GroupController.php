<?php

namespace App\Http\Controllers\Backend;

use App\Group;
use App\User;
use Illuminate\Http\Request;

class GroupController extends HtmlController
{

    public function __construct() {
        parent::__construct();
        $this->vars['title'] = 'Управление группами пользователей';
        $this->vars['titleSmall'] = 'Здесь вы можете изменять группы пользователей и их права доступа';
    }

    public function index()
    {
        $this->vars['groups'] = Group::all();
        $this->vars['groups'] = $this->deleteMajorGroups($this->vars['user_info'], $this->vars['groups']);
        return view('admin.groups.index', $this->vars);
    }

    public function create()
    {
        return view('admin.groups.create&edit', $this->vars);
    }

    public function store(Request $request)
    {
        $fields = $request->all();
        $fields = $this->prepareData($fields);
        $group = new Group();
        $this->save($group, $fields, true);
        return redirect()->route('groups.index');
    }

    public function edit($id)
    {
        $this->vars['group'] = Group::with(['permissions'])->find($id);
        if(!checkUserAccess($this->vars['user_info'], getEntityAccess($this->vars['group']))) {
            return redirect()->back();
        }
        return view('admin.groups.create&edit', $this->vars);
    }

    public function update(Request $request, $id)
    {
        $fields = $request->all();
        $fields = $this->prepareData($fields);
        $group = Group::find($id);
        $this->save($group, $fields, false);
        return redirect()->route('groups.index');
    }

    public function destroy($id)
    {
        $group = Group::find($id);
        $group->users()->detach();
        $group->permissions()->detach();
        $group->delete();
        return redirect()->route('groups.index');
    }

    /**
     * @param $userId - id пользователя
     *
     * @return возвращает форму добавления группы
     *
     * Вывод формы добавления группы пользователю
     */
    public function formAdd($userId) {
        $this->vars['groups'] = Group::all();
        $this->vars['user'] = User::find($userId);
        $this->vars['groups'] = $this->prepareGroups($this->vars['user'], $this->vars['groups']);
        return view('admin.users.groupAdd&Change', $this->vars);
    }

    /**
     * @param $userId - id пользователя
     * @param $groupId - id роли
     *
     * @return возвращает форму редактирования группы
     *
     * Вывод формы редактирования группы пользователя
     */
    public function formChange($userId, $groupId) {
        $this->vars['groups'] = Group::all();
        $this->vars['group'] = $this->vars['groups']->where('id', $groupId)->first();
        if(!checkUserAccess($this->vars['user_info'], getEntityAccess($this->vars['group']))) {
            return redirect()->back();
        }
        if(getUserAccess($this->vars['user_info']) < getUserAccess(User::find($userId))) {
            return redirect()->back();
        }
        $this->vars['user'] = User::find($userId);
        $this->vars['groups'] = $this->prepareGroups($this->vars['user'], $this->vars['groups']);
        return view('admin.users.groupAdd&Change', $this->vars);
    }

    /**
     * @param $userId - id пользователя
     * @param $groupId - id роли
     *
     * @return редирект обратно на страницу редактирования
     *
     * Функция удаления роли у пользователя
     */
    public function groupDelete($userId, $groupId) {
        $group = Group::find($groupId);
        if(checkUserAccess($this->vars['user_info'], getEntityAccess($group))) {
            return redirect()->back();
        }
        if(getUserAccess($this->vars['user_info']) < getUserAccess(User::find($userId))) {
            return redirect()->back();
        }
        $group->users()->detach($userId);
        return redirect()->route('users.edit', [$userId]);
    }

    /**
     * @param $userId - id пользователя
     * @param Request $request - полученный запрос
     * @return \Illuminate\Http\RedirectResponse - редиректит на редактирование пользователя
     */
    public function groupAdd($userId, Request $request) {
        $user = User::find($userId);
        $user->groups()->attach($request->group_id);
        return redirect()->route('users.edit', [$userId]);
    }

    /**
     * @param $userId - id пользователя
     * @param $groupId - id роли
     * @param Request $request - полученный запрос
     * @return \Illuminate\Http\RedirectResponse - редиректит на редактирование пользователя
     */
    public function groupChange($userId, $groupId, Request $request) {
        $user = User::find($userId);
        $user->groups()->updateExistingPivot($groupId, [
            'group_id' => $request->group_id
        ]);
        return redirect()->route('users.edit', [$userId]);
    }

    protected function prepareGroups(User $user, $groups) {
        foreach($user->groups as $userGroup) {
            foreach($groups as $id=>$group) {
                if($group->id == $userGroup->id) {
                    unset($groups[$id]);
                }
            }
        }
        $groups = $this->deleteMajorGroups($this->vars['user_info'], $groups);
        return $groups;
    }

    protected function deleteMajorGroups(User $user, $groups) {
        foreach ($groups as $id => $group) {
            if(!checkUserAccess($user, getEntityAccess($group))) {
                unset($groups[$id]);
            }
        }
        return $groups;
    }

}
