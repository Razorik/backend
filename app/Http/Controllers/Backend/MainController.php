<?php

namespace App\Http\Controllers\Backend;


class MainController extends HtmlController
{
    public function index() {
        $this->vars['title'] = 'Главная страница';
        $this->vars['titleSmall'] = 'Главная страница панели управления';
        return view('admin.dashboard.html', $this->vars);
    }
}
