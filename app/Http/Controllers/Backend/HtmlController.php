<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Table;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HtmlController extends Controller
{

    protected $vars;
    protected $user;

    public function __construct() {
        $this->vars['tables'] = Table::with(['fields'])->get();
        $this->middleware(function ($request, $next) {
//
            if(Auth::check()) {
                $this->vars['user_info'] = User::with(['roles', 'groups', 'roles.permissions', 'groups.permissions'])->find(Auth::user()->id);
            } else {
                return redirect('/admin/login');
            }
            return $next($request);
        });
    }

    protected function save($thing, $fields, $isNew = false) {
        foreach ($fields as $key => $value) {
            $thing->$key = $value;
        }
        if ($isNew) {
            $thing->save();
        } else {
            $thing->update();
        }
    }

    protected function getAssociateArray($array, $names) {
        $newArray = [];
        foreach($names as $key=>$name) {
            $newArray[$name] = $array[$key];
        }
        return $newArray;
    }

    protected function prepareData($fields) {
        if(isset($fields['_token'])) {
            unset($fields['_token']);
        }
        if(isset($fields['_method'])) {
            unset($fields['_method']);
        }
        return $fields;
    }
}
