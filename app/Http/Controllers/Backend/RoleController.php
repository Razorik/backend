<?php

namespace App\Http\Controllers\Backend;

use App\Role;
use App\User;
use Illuminate\Http\Request;

class RoleController extends HtmlController
{

    public function __construct() {
        parent::__construct();
        $this->vars['title'] = 'Управление ролями пользователей';
        $this->vars['titleSmall'] = 'Здесь вы можете изменять роли пользователей и их права доступа';
    }

    public function index()
    {
        $this->vars['roles'] = Role::all();
        $this->vars['roles'] = $this->deleteMajorRoles($this->vars['user_info'], $this->vars['roles']);
        return view('admin.roles.index', $this->vars);
    }

    public function create()
    {
        return view('admin.roles.create&edit', $this->vars);
    }

    public function store(Request $request)
    {
        $fields = $request->all();
        $fields = $this->prepareData($fields);
        $role = new Role();
        $this->save($role, $fields, true);
        return redirect()->route('roles.index');
    }

    public function edit($id)
    {
        $this->vars['role'] = Role::with(['permissions'])->find($id);
        if(!checkUserAccess($this->vars['user_info'], getEntityAccess($this->vars['role']))) {
            return redirect()->back();
        }
        return view('admin.roles.create&edit', $this->vars);
    }

    public function update(Request $request, $id)
    {
        $fields = $request->all();
        $fields = $this->prepareData($fields);
        $role = Role::find($id);
        $this->save($role, $fields, false);
        return redirect()->route('roles.index');
    }

    public function destroy($id)
    {
        $role = Role::find($id);
        $role->users()->detach();
        $role->permissions()->detach();
        $role->delete();
        return redirect()->route('roles.index');
    }

    /**
     * @param $userId - id пользователя
     *
     * @return возвращает форму добавления роли
     *
     * Вывод формы добавления роли пользователю
     */
    public function formAdd($userId) {
        $this->vars['roles'] = Role::all();
        $this->vars['user'] = User::with(['roles'])->find($userId);
        $this->vars['roles'] = $this->prepareRoles($this->vars['user'], $this->vars['roles']);
        return view('admin.users.roleAdd&Change', $this->vars);
    }

    /**
     * @param $userId - id пользователя
     * @param $roleId - id роли
     *
     * @return возвращает форму редактирования роли
     *
     * Вывод формы редактирования роли пользователя
     */
    public function formChange($userId, $roleId) {
        $this->vars['roles'] = Role::all();
        $this->vars['role'] = $this->vars['roles']->where('id', $roleId)->first();
        if(!checkUserAccess($this->vars['user_info'], getEntityAccess($this->vars['role']))) {
            return redirect()->back();
        }
        if(getUserAccess($this->vars['user_info']) < getUserAccess(User::find($userId))) {
            return redirect()->back();
        }
        $this->vars['user'] = User::with(['roles'])->find($userId);
        $this->vars['roles'] = $this->prepareRoles($this->vars['user'], $this->vars['roles']);
        return view('admin.users.roleAdd&Change', $this->vars);
    }

    /**
     * @param $userId - id пользователя
     * @param $roleId - id роли
     *
     * @return редирект обратно на страницу редактирования
     *
     * Функция удаления роли у пользователя
     */
    public function roleDelete($userId, $roleId) {
        $role = Role::find($roleId);
        if(!checkUserAccess($this->vars['user_info'], getEntityAccess($role))) {
            return redirect()->back();
        }
        if(getUserAccess($this->vars['user_info']) < getUserAccess(User::find($userId))) {
            return redirect()->back();
        }
        $role->users()->detach($userId);
        return redirect()->route('users.edit', [$userId]);
    }

    /**
     * @param $userId - id пользователя
     * @param Request $request - полученный запрос
     * @return \Illuminate\Http\RedirectResponse - редиректит на редактирование пользователя
     */
    public function roleAdd($userId, Request $request) {
        $user = User::find($userId);
        $user->roles()->attach($request->role_id);
        return redirect()->route('users.edit', [$userId]);
    }

    /**
     * @param $userId - id пользователя
     * @param $roleId - id роли
     * @param Request $request - полученный запрос
     * @return \Illuminate\Http\RedirectResponse - редиректит на редактирование пользователя
     */
    public function roleChange($userId, $roleId, Request $request) {
        $user = User::find($userId);
        $user->roles()->updateExistingPivot($roleId, [
            'role_id' => $request->role_id
        ]);
        return redirect()->route('users.edit', [$userId]);
    }

    protected function prepareRoles(User $user, $roles) {
        foreach($user->roles as $userRole) {
            foreach($roles as $id=>$role) {
                if($role->id == $userRole->id) {
                    unset($roles[$id]);
                }
            }
        }
        $roles = $this->deleteMajorRoles($this->vars['user_info'], $roles);
        return $roles;
    }

    protected function deleteMajorRoles(User $user, $roles) {
        foreach ($roles as $id => $role) {
            if(!checkUserAccess($user, getEntityAccess($role))) {
                unset($roles[$id]);
            }
        }
        return $roles;
    }

}
