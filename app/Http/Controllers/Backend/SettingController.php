<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Setting;

class SettingController extends HtmlController
{

    public function __construct()
    {
        parent::__construct();
        $this->vars['title'] = 'Настройки';
        $this->vars['titleSmall'] = 'Настройки вашего сайта';
    }

    public function index()
    {
        $this->vars['settings'] = Setting::all();
        return view('admin.settings.index', $this->vars);
    }

    public function create()
    {
        return view('admin.settings.create&edit', $this->vars);
    }

    public function store(Request $request)
    {
        $fields = $request->all();
        $setting = new Setting();
        $this->save($setting, $fields, true);
        return redirect()->route('settings.index');
    }

    public function edit($key)
    {
        $this->vars['setting'] = Setting::getByKey($key);
        if (isset($this->vars['setting'])) {
            return view('admin.settings.create&edit', $this->vars);
        } else {
            return redirect('/admin/settings');
        }

    }

    public function update(Request $request, $key)
    {
        $fields = $request->all();
        $setting = Setting::getByKey($key);
        if (isset($setting)) {
            $this->save($setting, $fields);
        } else {
            $this->save(new Setting(), $fields, true);
        }
        return redirect()->route('settings.index');
    }

    public function destroy($key)
    {
        $setting = Setting::getByKey($key);
        if (isset($setting)) {
            $setting->delete();
            return redirect()->back();
        } else {
            return redirect('/admin/settings');
        }
    }
}
