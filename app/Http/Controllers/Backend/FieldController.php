<?php

namespace App\Http\Controllers\Backend;

use App\Field;
use App\Table;
use Illuminate\Http\Request;
use App\Http\Helpers\TableEditor;

class FieldController extends HtmlController
{

    public function __construct()
    {
        parent::__construct();
        $this->vars['title'] = 'Поля таблиц вашего сайта';
        $this->vars['smallTitle'] = 'Управление полями таблиц вашего сайта.';
    }

    public function create($tableId)
    {
        $this->vars['table'] = Table::find($tableId);
        return view('admin.manage.fields.create&edit', $this->vars);
    }

    public function store($tableId, Request $request)
    {
        $fields = $request->all();
        $field = new Field();
        $fields = $this->prepareData($fields);
        $this->save($field, $fields, true);
        $te = new TableEditor($field);
        $te->alterTable()->addField()->run();
        return redirect()->route('manage.edit', ['id' => $tableId]);
    }

    public function edit($tableId, $id)
    {
        $this->vars['field'] = Field::find($id);
        return view('admin.manage.fields.create&edit', $this->vars);
    }

    public function update($tableId, Request $request, $id)
    {
        $fields = $request->all();
        $fields = $this->prepareData($fields);
        $field = Field::find($id);
        $this->save($field, $fields, false);
        $te = new TableEditor($field);
        $te->alterTable()->editField()->run();
        return redirect()->route('manage.edit', ['id' => $tableId]);
    }

    public function destroy($tableId, $id)
    {
        $field = Field::find($id);
        $te = new TableEditor($field);
        $te->alterTable()->deleteField()->run();
        $field->delete();
        return redirect()->back();
    }

    public function prepareData($fields)
    {
        $fields = parent::prepareData($fields);
        if (isset($fields['nullable'])) {
            $fields['nullable'] == 'on' ? $fields['nullable'] = 1 : $fields['nullable'] = 0;
        } else { $field['nullable'] = 0; }
        if (isset($fields['required'])) {
            $fields['required'] == 'on' ? $fields['required'] = 1 : $fields['required'] = 0;
        } else { $field['required'] = 0; }
        if (isset($fields['show_in_table'])) {
            $fields['show_in_table'] == 'on' ? $fields['show_in_table'] = 1 : $fields['show_in_table'] = 0;
        } else { $fields['show_in_table'] = 0; }
        if (isset($fields['show_in_form'])) {
            $fields['show_in_form'] == 'on' ? $fields['show_in_form'] = 1 : $fields['show_in_form'];
        } else { $fields['show_in_form'] = 0; }
        return $fields;
    }

}
