<?php

namespace App\Http\Controllers\Backend;

use App\Field;
use App\Table;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TableController extends HtmlController
{

    public function __construct()
    {
        parent::__construct();
        $this->vars['title'] = 'Таблицы';
        $this->vars['titleSmall'] = 'Управление таблицами вашего сайта';
    }

    public function index()
    {
        return view('admin.manage.index', $this->vars);
    }

    public function create()
    {
        return view('admin.manage.create&edit', $this->vars);
    }

    public function store(Request $request)
    {
        $fields = $request->all();
        $table = new Table();
        $fields = $this->prepareData($fields);
        $this->save($table, $fields, true);
        $this->createTable($fields);
        $this->addDefaultFields($table);
        return redirect()->route('manage.index');;
    }

    public function edit($id)
    {
        $this->vars['table'] = Table::getByKey($id);
        if (isset($this->vars['table'])) {
            return view('admin.manage.create&edit', $this->vars);
        } else {
            return redirect('/admin/manage');
        }

    }

    public function update(Request $request, $id)
    {
        $fields = $request->all();
        $table = Table::getByKey($id);
        $fields = $this->prepareData($fields);
        if (isset($table)) {
            $this->save($table, $fields);
        } else {
            $table = new Table();
            $this->save($table, $fields, true);
            $this->createTable($fields);
            $this->addDefaultFields($table);
        }
        return redirect()->route('manage.index');
    }

    public function destroy($id)
    {
        $table = Table::getByKey($id);
        if (isset($table)) {
            $this->deleteFields($table);
            $this->deleteTable($table);
            $table->delete();
            return redirect()->back();
        } else {
            return redirect('/admin/manage');
        }
    }

    public function prepareData($fields) {
        $fields = parent::prepareData($fields);
        if(isset($fields['show_table'])) {
            $fields['show_table'] = 1;
        } else {
            $fields['show_table'] = 0;
        }
        if(isset($fields['name'])) {
            $fields['name'] = str_slug(strtolower($fields['name']));
        }
        return $fields;
    }

    public function createTable($fields) {
        $query = 'CREATE TABLE ' . $fields['name'] . ' (
	id int AUTO_INCREMENT,
	parent_id int,
	name varchar(255),
	`order` int,
	active int,
	created_at datetime,
	updated_at datetime,
	deleted_at datetime,
	PRIMARY KEY (id)
);';
        DB::statement($query);
    }

    public function addDefaultFields(Table $table) {
        $defaultFields = Field::$defaultFields;
        foreach($defaultFields as $field) {
            $newField = new Field();
            $newField->parent_id = $table->id;
            $newField->name = $field;
            $configs = config('default_fields.' . $field);
            foreach($configs as $key=>$config) {
                $newField->$key = $config;
            }
            $newField->save();
        }
    }

    public function deleteTable(Table $table) {
        $query = 'DROP TABLE ' . $table->name;
        DB::statement($query);
    }

    public function deleteFields(Table $table) {
        $fields = Field::where('parent_id', $table->id)->get();
        foreach($fields as $field) {
            $field->delete();
        }
    }

}
