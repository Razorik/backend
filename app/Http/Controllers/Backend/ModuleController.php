<?php

namespace App\Http\Controllers\Backend;

use App\Http\Helpers\FieldSaver;
use App\Http\Helpers\FormCreator;
use App\Module;
use App\Table;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\URL;

class ModuleController extends HtmlController
{

    protected $module;
    protected $currentRoute;
    protected $fc;
    protected $linksToBack;
    protected $fs;

    public function __construct() {
        parent::__construct();
        $this->module = new Module;
        $this->currentRoute = explode('.', Route::currentRouteName());
        $this->currentRoute = $this->getAssociateArray($this->currentRoute, ['table','method']);
        $this->vars['title'] = 'Управление содержимым модуля ' . $this->currentRoute['table'];
        $this->vars['titleSmall'] = 'На этой странице вы можете изменять содержимое модуля ' . $this->currentRoute['table'];
        $this->module->setModule($this->currentRoute['table']);
        $this->vars['table'] = $this->vars['tables']->where('name', $this->currentRoute['table'])->first();
        $this->fc = new FormCreator();
        $this->fs = new FieldSaver();
    }

    public function index()
    {
        session(['linksToBack' => [URL::current()]]);
        $this->vars['entries'] = $this->module->get();
        return view('admin.modules.index', $this->vars);
    }

    public function create()
    {
        $this->checkLink();
        $this->vars['formContent'] = $this->createForm($this->vars['table']);
        return view('admin.modules.create&edit', $this->vars);
    }

    public function show($id) {
        $this->checkLink();
        $this->vars['entries'] = $this->module->where('parent_id', $id)->get();
        return view('admin.modules.index', $this->vars);
    }

    public function store(Request $request)
    {
        $link = session()->get('linksToBack')[count(session()->get('linksToBack'))-2];
        $fields = $request->all();
        $entry = new Module();
        $entry->setModule($this->currentRoute['table']);
        $this->save($entry, $fields, true);
        return redirect($link);
    }

    public function edit($id)
    {
        $this->checkLink();
        $this->vars['entry'] = $this->module->where('id', $id)->first();
        $this->vars['parents'] = $this->vars['tables']->where('parent_id', $this->vars['table']->id)->all();
        $this->vars['formContent'] = $this->createForm($this->vars['table'], $this->vars['entry']);
        return view('admin.modules.create&edit', $this->vars);
    }

    public function update(Request $request, $id)
    {
        $link = session()->get('linksToBack')[count(session()->get('linksToBack'))-2];
        $entry = $this->getEntryById($id);
        $fields = $request->all();
        $this->save($entry, $fields, false);
        return redirect($link);
    }

    public function destroy($id)
    {
        $link = session()->get('linksToBack')[count(session()->get('linksToBack'))-2];
        $entry = $this->getEntryById($id);
        $entry->delete();
        return redirect($link);
    }

    public function getEntryById($id) {
        $entry = $this->module->find($id);
        $entry->setModule($this->currentRoute['table']);
        return $entry;
    }

    public function createForm(Table $table, Module $entry = null) {
        $formContent = '';
        foreach($table->fields->where('show_in_form', 1)->sortBy('order') as $field) {
            $formContent .= $this->fc->create($field, $entry);
        }
        return $formContent;
    }

    private function addLink() {
        $this->linksToBack = session()->get('linksToBack');
        array_push($this->linksToBack, URL::current());
        session(['linksToBack' => $this->linksToBack]);
    }

    private function deleteLink() {
        $this->linksToBack = session()->get('linksToBack');
        array_pop($this->linksToBack);
        session(['linksToBack' => $this->linksToBack]);
    }

    private function checkLink() {
        $links = session()->get('linksToBack');
        if($links[count($links)-1] != URL::current()) {
            if(isset($links[count($links)-2]) && $links[count($links)-2] == URL::current()) {
                $this->deleteLink();
            } else {
                $this->addLink();
            }
        }
    }

    public function save($thing, $fields, $isNew = false) {
        foreach($fields as $key=>$value) {
            if ($key != '_token' && $key != '_method') {
                $field = $this->vars['table']->fields->where('name', $key)->first();
                $thing->$key = $this->fs->saveField($this->vars['table'], $field, $value);
            }
        }
        if ($isNew) {
            $thing->save();
        } else {
            $thing->update();
        }
    }

}
