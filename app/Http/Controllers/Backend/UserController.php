<?php

namespace App\Http\Controllers\Backend;

use App\User;
use Illuminate\Http\Request;

class UserController extends HtmlController
{

    public function __construct() {
        parent::__construct();
        $this->vars['title'] = 'Управление пользователями сайта';
        $this->vars['titleSmall'] = 'Здесь вы можете редактировать/изменять данные о пользователях вашего сайта';
    }

    public function index()
    {
        $this->vars['users'] = User::limit(100)->get();
        return view('admin.users.index', $this->vars);
    }

    public function create()
    {
        return view('admin.users.create&edit',$this->vars);
    }

    public function store(Request $request)
    {
        $fields = $request->all();
        $fields = $this->prepareData($fields);
        $user = new User();
        $this->save($user, $fields, true);
        return redirect()->route('users.index');
    }

    public function edit($id)
    {
        $this->vars['user'] = User::with(['roles', 'groups', 'roles.permissions', 'groups.permissions'])->find($id);
        $this->vars['editable'] = getUserAccess($this->vars['user_info']) >= getUserAccess($this->vars['user']) ? '' : 'disabled';
        return view('admin.users.create&edit', $this->vars);
    }

    public function update(Request $request, $id)
    {
        $fields = $request->all();
        $fields = $this->prepareData($fields);
        $user = User::find($id);
        $this->save($user, $fields, false);
        return redirect()->route('users.index');
    }

    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();
        return redirect()->route('users.index');
    }

    public function prepareData($fields) {
        $fields = parent::prepareData($fields);
        if(isset($fields['password']) && strlen($fields['password']) > 0) {
            $fields['password'] = bcrypt($fields['password']);
        } else {
            unset($fields['password']);
        }
        return $fields;
    }
}
