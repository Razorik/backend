<?php

namespace App\Http\Middleware;

use App\User;
use Closure;
use Illuminate\Support\Facades\Auth;

class CheckAccess
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $accessLevel)
    {
        $user = User::with(['roles', 'roles.permissions', 'groups', 'groups.permissions'])->find(Auth::user()->id);
        $can = false;
        foreach($user->roles as $role) {
            if(count($role->permissions->where('value', '>=', $accessLevel)->all()) > 0) {
                $can = true;
                break;
            }
        }
        if(!$can) {
            foreach($user->groups as $group) {
                if(count($group->permissions->where('value', '>=', $accessLevel)->all()) > 0) {
                    $can = true;
                    break;
                }
            }
        }
        if($can) {
            return $next($request);
        } else {
            return redirect()->back();
        }
    }
}
