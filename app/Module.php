<?php

namespace App;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

class Module extends Model
{

    use Sluggable;

    protected $table = '';

    public $sluggable = [
        'slug' => [
            'source' => 'name'
        ]
    ];

    public function setModule($name) {
        $this->table = $name;
    }

    public function sluggable() {
        return $this->sluggable;
    }

}
