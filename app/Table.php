<?php

namespace App;


class Table extends BaseModel
{
    protected $table = '_tables';

    public function tables() {
        return self::belongsTo(Table::class, 'parent_id', 'id');
    }

    public function fields() {
        return self::hasMany(Field::class, 'parent_id', 'id');
    }

}
