<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    protected $table = 'groups';

    public function users() {
        return self::belongsToMany(User::class, 'users_groups');
    }

    public function permissions() {
        return self::belongsToMany(Permission::class, 'groups_permissions');
    }
}
