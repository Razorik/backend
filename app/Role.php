<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $table = 'roles';

    public function users() {
        return self::belongsToMany(User::class, 'users_roles');
    }

    public function permissions() {
        return self::belongsToMany(Permission::class, 'roles_permissions');
    }
}
