<?php

namespace App;


class Field extends BaseModel
{
    protected $table = '_fields';

    public static $defaultFields = ['id', 'parent_id', 'name', 'order', 'active'];

    public function table() {
        return self::belongsTo(Table::class, 'parent_id', 'id');
    }

}
