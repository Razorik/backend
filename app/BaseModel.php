<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BaseModel extends Model
{

    public static $key = 'id';

    public static function getByKey($value) {
        return self::where(self::$key, $value)->first();
    }
}
