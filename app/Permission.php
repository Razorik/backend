<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    protected $table = 'permissions';

    public function groups() {
        return self::belongsToMany(Group::class, 'groups_permissions');
    }

    public function roles() {
        return self::belongsToMany(Role::class, 'roles_permissions');
    }

}
