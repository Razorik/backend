/*
Navicat MySQL Data Transfer

Source Server         : mysql
Source Server Version : 100121
Source Host           : localhost:3306
Source Database       : accentclub

Target Server Type    : MYSQL
Target Server Version : 100121
File Encoding         : 65001

Date: 2017-11-11 02:36:17
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for categories
-- ----------------------------
DROP TABLE IF EXISTS `categories`;
CREATE TABLE `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  `active` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of categories
-- ----------------------------
INSERT INTO `categories` VALUES ('2', 'новости', '1', '1', '2017-09-29 19:54:13', '2017-10-05 19:19:41', null, 'novosti');
INSERT INTO `categories` VALUES ('3', 'Статьи', null, '1', '2017-09-29 20:12:42', '2017-09-29 20:12:42', null, null);

-- ----------------------------
-- Table structure for groups
-- ----------------------------
DROP TABLE IF EXISTS `groups`;
CREATE TABLE `groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at$group` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of groups
-- ----------------------------
INSERT INTO `groups` VALUES ('1', 'DevGroup', '2017-10-09 00:43:00', '2017-10-08 19:43:00', '2017-10-09 00:43:00');
INSERT INTO `groups` VALUES ('2', 'AdminGroup', '2017-10-08 19:43:15', '2017-10-08 19:43:15', null);

-- ----------------------------
-- Table structure for groups_permissions
-- ----------------------------
DROP TABLE IF EXISTS `groups_permissions`;
CREATE TABLE `groups_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) DEFAULT NULL,
  `permission_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `group_id` (`group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of groups_permissions
-- ----------------------------
INSERT INTO `groups_permissions` VALUES ('7', '1', '1');
INSERT INTO `groups_permissions` VALUES ('9', '2', '2');

-- ----------------------------
-- Table structure for permissions
-- ----------------------------
DROP TABLE IF EXISTS `permissions`;
CREATE TABLE `permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `value` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of permissions
-- ----------------------------
INSERT INTO `permissions` VALUES ('1', 'developer', '1000', null, null, null);
INSERT INTO `permissions` VALUES ('2', 'admin', '950', null, null, null);
INSERT INTO `permissions` VALUES ('3', 'moderator', '500', '2017-11-10 20:23:30', '2017-11-10 20:23:30', null);

-- ----------------------------
-- Table structure for posts
-- ----------------------------
DROP TABLE IF EXISTS `posts`;
CREATE TABLE `posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  `active` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `content` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of posts
-- ----------------------------
INSERT INTO `posts` VALUES ('3', '2', 'лоалопапа', null, '1', '2017-09-29 20:05:45', '2017-09-29 20:05:45', null, null);
INSERT INTO `posts` VALUES ('4', '3', 'Статья в маркетинге', null, '1', '2017-09-29 20:13:22', '2017-09-29 20:13:22', null, null);
INSERT INTO `posts` VALUES ('5', '1', 'Саня лох', null, '1', '2017-10-03 20:03:56', '2017-10-03 20:03:56', null, 'Скня лох потому что я так думаю');

-- ----------------------------
-- Table structure for roles
-- ----------------------------
DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of roles
-- ----------------------------
INSERT INTO `roles` VALUES ('1', 'Developer', null, null, null);
INSERT INTO `roles` VALUES ('2', 'Admin', null, null, null);
INSERT INTO `roles` VALUES ('3', 'Moderator', null, null, null);
INSERT INTO `roles` VALUES ('4', 'User', '2017-10-09 00:43:32', '2017-10-08 19:43:32', '2017-10-09 00:43:32');

-- ----------------------------
-- Table structure for roles_permissions
-- ----------------------------
DROP TABLE IF EXISTS `roles_permissions`;
CREATE TABLE `roles_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) DEFAULT NULL,
  `permission_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `role_id` (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of roles_permissions
-- ----------------------------
INSERT INTO `roles_permissions` VALUES ('1', '1', '1');
INSERT INTO `roles_permissions` VALUES ('2', '2', '2');
INSERT INTO `roles_permissions` VALUES ('3', '3', '3');

-- ----------------------------
-- Table structure for rubrics
-- ----------------------------
DROP TABLE IF EXISTS `rubrics`;
CREATE TABLE `rubrics` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  `active` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of rubrics
-- ----------------------------
INSERT INTO `rubrics` VALUES ('1', '2', 'спорт', null, '1', '2017-09-29 19:55:50', '2017-09-29 19:55:50', null);
INSERT INTO `rubrics` VALUES ('2', '2', 'политика', null, '1', '2017-09-29 19:58:26', '2017-09-29 19:58:26', null);
INSERT INTO `rubrics` VALUES ('3', '3', 'Маркетинг', null, '1', '2017-09-29 20:13:03', '2017-09-29 20:13:03', null);

-- ----------------------------
-- Table structure for settings
-- ----------------------------
DROP TABLE IF EXISTS `settings`;
CREATE TABLE `settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `key` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `group` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `created_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of settings
-- ----------------------------
INSERT INTO `settings` VALUES ('1', 'URL сайта', 'site_url', 'local.accentclub.ru', 'Основные', 'Указывается URL сайта без http:// и www.', '2017-09-21 01:47:23', '2017-09-20 20:47:23');
INSERT INTO `settings` VALUES ('2', 'Email', 'email', 'admin@local.accentclub.ru', 'Основные', 'Указывается адрес электронной почты (например: example@example.ru)', '2017-09-21 22:29:59', '2017-09-21 17:29:59');
INSERT INTO `settings` VALUES ('5', 'Копирайт', 'copyright', 'AccentClub (c) Copyright', 'Основные', 'Настройка меняет надпись копирайта на сайте.', '2017-09-20 20:48:21', '2017-09-20 20:48:21');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `remember_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`) USING BTREE,
  UNIQUE KEY `email` (`email`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('1', 'Superadmin', 'imelkozyorov@gmail.com', '$2y$10$HGpHV0VtFIG37fmzc.RtROlKGheMBFwuoxabX68mLnrZAzMgi.3Ay', '7a0BgJY0jpvy0ryEFbtWvPk5IRicGcgtfiw051ZzN5dC4SPhbRzw1ORThLeS', '2017-11-11 02:22:15', '2017-11-10 21:22:15', '2017-11-11 02:22:15');
INSERT INTO `users` VALUES ('2', 'Admin', 'alex@accentclub.ru', '$2y$10$YiX0wkZCZ8W.fWdrAJGFFOxbMTiv./u9195l../3iEYe/kBryF/g.', 'nhDTvZnBTquBMh6rxDSQZcdj5aVklXWqil24i4E7a6FDEiiM4EYxvWlmBnxa', null, null, null);
INSERT INTO `users` VALUES ('3', 'Ivan', 'ivan@accentclub.ru', '$2y$10$UIrJfISoMrITTH3okJg/LeEqBjf/.i4RNznMxIX5sbVIEfSizlpqu', 'gtWj9PN4Nh9tNl5TyeCVTRnxUzNIuTkEdglLLJwSYE16eYGhGG9bxRvRzjJs', '2017-11-11 02:35:08', '2017-11-11 02:35:08', '2017-11-11 02:35:08');

-- ----------------------------
-- Table structure for users_groups
-- ----------------------------
DROP TABLE IF EXISTS `users_groups`;
CREATE TABLE `users_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `group_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `group_id` (`group_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of users_groups
-- ----------------------------
INSERT INTO `users_groups` VALUES ('4', '1', '1');
INSERT INTO `users_groups` VALUES ('7', '2', '2');
INSERT INTO `users_groups` VALUES ('8', '3', '2');

-- ----------------------------
-- Table structure for users_roles
-- ----------------------------
DROP TABLE IF EXISTS `users_roles`;
CREATE TABLE `users_roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `role_id` (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of users_roles
-- ----------------------------
INSERT INTO `users_roles` VALUES ('2', '2', '2');
INSERT INTO `users_roles` VALUES ('13', '1', '1');
INSERT INTO `users_roles` VALUES ('14', '3', '2');

-- ----------------------------
-- Table structure for _fields
-- ----------------------------
DROP TABLE IF EXISTS `_fields`;
CREATE TABLE `_fields` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `d_type` varchar(11) COLLATE utf8_unicode_ci DEFAULT NULL,
  `placeholder` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `default` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `options` text COLLATE utf8_unicode_ci,
  `order` int(11) DEFAULT NULL,
  `nullable` int(11) DEFAULT NULL,
  `required` int(11) DEFAULT NULL,
  `show_in_table` int(11) DEFAULT NULL,
  `show_in_form` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`) USING BTREE,
  KEY `parent_id` (`parent_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=67 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of _fields
-- ----------------------------
INSERT INTO `_fields` VALUES ('49', '24', 'ID', 'id', 'text', 'int', 'Введите порядковый номер', 'В поле записывается порядковый номер', null, null, '1', '0', '1', '1', '0', '2017-09-30 01:16:46', '2017-09-29 20:16:46', '2017-09-30 01:16:46');
INSERT INTO `_fields` VALUES ('51', '24', 'Название', 'name', 'text', 'varchar', 'Введите название', 'В поле хранится название элемента', null, null, '3', '0', '1', '1', '1', '2017-09-30 01:16:53', '2017-09-29 20:16:53', '2017-09-30 01:16:53');
INSERT INTO `_fields` VALUES ('52', '24', 'Сортировка', 'order', 'text', 'int', 'Введите значение для сортировки', 'В поле хранится значение для сортировки', null, null, '4', '0', '1', '0', '0', '2017-09-30 00:54:59', '2017-09-29 19:54:59', '2017-09-30 00:54:59');
INSERT INTO `_fields` VALUES ('53', '24', 'Активность', 'active', 'select', 'int', 'Выберите активен элемент или нет', 'В поле хранится значение отображающее активность элемента', null, '{\"list\":{\"0\":\"Нет\", \"1\":\"Да\"}}', '5', '0', '1', '0', '1', '2017-09-29 02:11:26', '2017-09-28 21:11:26', '2017-09-29 02:11:26');
INSERT INTO `_fields` VALUES ('54', '25', 'ID', 'id', 'text', 'int', 'Введите порядковый номер', 'В поле записывается порядковый номер', '', '', '1', '0', '1', '1', '0', '2017-09-26 17:19:12', '2017-09-26 17:19:12', null);
INSERT INTO `_fields` VALUES ('55', '25', 'Родительский элемент', 'parent_id', 'select', 'int', 'Выберите родительский элемент', 'В поле отображается элемент который является родительским для данного', null, '{\r\n\"table\": {\r\n\"name\": \"categories\",\r\n\"value\": \"id\",\r\n\"text\": \"name\"\r\n}\r\n}', '2', '1', '0', '1', '1', '2017-09-29 02:12:30', '2017-09-28 21:12:30', '2017-09-29 02:12:30');
INSERT INTO `_fields` VALUES ('56', '25', 'Название', 'name', 'text', 'varchar', 'Введите название', 'В поле хранится название элемента', '', '', '3', '0', '1', '1', '1', '2017-09-26 17:19:12', '2017-09-26 17:19:12', null);
INSERT INTO `_fields` VALUES ('57', '25', 'Сортировка', 'order', 'text', 'int', 'Введите значение для сортировки', 'В поле хранится значение для сортировки', null, null, '4', '0', '1', '0', '0', '2017-09-30 00:55:07', '2017-09-29 19:55:07', '2017-09-30 00:55:07');
INSERT INTO `_fields` VALUES ('58', '25', 'Активность', 'active', 'select', 'int', 'Выберите активен элемент или нет', 'В поле хранится значение отображающее активность элемента', null, '{\r\n\"list\":\r\n{\r\n\"0\":\"Нет\", \r\n\"1\":\"Да\"\r\n}\r\n}', '5', '0', '1', '0', '1', '2017-09-29 02:13:37', '2017-09-28 21:13:37', '2017-09-29 02:13:37');
INSERT INTO `_fields` VALUES ('60', '26', 'ID', 'id', 'text', 'int', 'Введите порядковый номер', 'В поле записывается порядковый номер', null, '', '1', '0', '1', '1', '0', '2017-09-29 19:47:16', '2017-09-29 19:47:16', null);
INSERT INTO `_fields` VALUES ('61', '26', 'Родительский элемент', 'parent_id', 'multiselect', 'int', 'Выберите родительский элемент', 'В поле отображается элемент который является родительским для данного', null, '{\r\n\"table\": {\r\n\"name\": \"rubrics\",\r\n\"value\": \"id\",\r\n\"text\": \"name\"\r\n}\r\n}', '2', '1', '0', '0', '1', '2017-09-30 01:04:36', '2017-09-29 20:04:36', '2017-09-30 01:04:36');
INSERT INTO `_fields` VALUES ('62', '26', 'Название', 'name', 'text', 'varchar', 'Введите название', 'В поле хранится название элемента', null, null, '1', '0', '1', '1', '1', '2017-10-03 16:02:58', '2017-10-03 20:02:58', '2017-10-03 16:02:58');
INSERT INTO `_fields` VALUES ('63', '26', 'Сортировка', 'order', 'text', 'int', 'Введите значение для сортировки', 'В поле хранится значение для сортировки', null, null, '4', '0', '1', '0', '0', '2017-09-30 00:48:38', '2017-09-29 19:48:38', '2017-09-30 00:48:38');
INSERT INTO `_fields` VALUES ('64', '26', 'Активность', 'active', 'select', 'int', 'Выберите активен элемент или нет', 'В поле хранится значение отображающее активность элемента', null, '{\"list\":{\"0\":\"Нет\", \"1\":\"Да\"}}', '5', '0', '1', '0', '1', '2017-09-30 00:57:11', '2017-09-29 19:57:11', '2017-09-30 00:57:11');
INSERT INTO `_fields` VALUES ('65', '26', 'Содержание', 'content', 'textarea', 'varchar', 'Введите содержание статьи', null, null, null, '2', null, null, '0', '1', '2017-10-03 16:01:25', '2017-10-03 20:01:25', '2017-10-03 16:01:25');
INSERT INTO `_fields` VALUES ('66', '24', 'Слаг', 'slug', 'slug', 'varchar', 'Введите слаг записи', null, null, null, '5', null, null, '0', '1', '2017-10-06 00:39:48', '2017-10-05 19:39:48', '2017-10-06 00:39:48');

-- ----------------------------
-- Table structure for _tables
-- ----------------------------
DROP TABLE IF EXISTS `_tables`;
CREATE TABLE `_tables` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `icon` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  `show_table` int(11) DEFAULT NULL,
  `custom_controller` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `count_fields` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`) USING BTREE,
  KEY `parent_id` (`parent_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of _tables
-- ----------------------------
INSERT INTO `_tables` VALUES ('24', null, 'Категории', 'categories', 'Категории сайта', 'fa fa-puzzle-piece', '1', '1', null, '10', '2017-09-30 01:04:03', '2017-09-29 20:04:03', null);
INSERT INTO `_tables` VALUES ('25', '24', 'Рубрики', 'rubrics', 'Рубрики раздела', 'fa fa-check-square-o', '2', '0', null, '10', '2017-10-03 15:53:04', '2017-10-03 19:53:04', null);
INSERT INTO `_tables` VALUES ('26', '25', 'Записи', 'posts', null, 'fa fa-bell-o', '3', '0', null, '10', '2017-10-03 15:54:03', '2017-10-03 19:54:03', null);
