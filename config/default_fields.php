<?php

return [

    'id' => [
        'title' => 'ID',
        'f_type' => 'text',
        'd_type' => 'int',
        'placeholder' => 'Введите порядковый номер',
        'description' => 'В поле записывается порядковый номер',
        'options' => '',
        'order' => 1,
        'nullable' => '0',
        'required' => '1',
        'show_in_table' => '1',
        'show_in_form' => '0',
    ],

    'parent_id' => [
        'title' => 'Родительский элемент',
        'f_type' => 'select',
        'd_type' => 'int',
        'placeholder' => 'Выберите родительский элемент',
        'description' => 'В поле отображается элемент который является родительским для данного',
        'options' => '',
        'order' => 2,
        'nullable' => '1',
        'required' => '0',
        'show_in_table' => '1',
        'show_in_form' => '1',
    ],

    'name' => [
        'title' => 'Название',
        'f_type' => 'text',
        'd_type' => 'varchar',
        'placeholder' => 'Введите название',
        'description' => 'В поле хранится название элемента',
        'options' => '',
        'order' => 3,
        'nullable' => '0',
        'required' => '1',
        'show_in_table' => '1',
        'show_in_form' => '1',
    ],

    'order' => [
        'title' => 'Сортировка',
        'f_type' => 'text',
        'd_type' => 'int',
        'placeholder' => 'Введите значение для сортировки',
        'description' => 'В поле хранится значение для сортировки',
        'options' => '',
        'order' => 4,
        'nullable' => '0',
        'required' => '1',
        'show_in_table' => '0',
        'show_in_form' => '1',
    ],

    'active' => [
        'title' => 'Активность',
        'f_type' => 'text',
        'd_type' => 'select',
        'placeholder' => 'Выберите активен элемент или нет',
        'description' => 'В поле хранится значение отображающее активность элемента',
        'options' => '{"list":{"0":"Нет", "1":"Да"}}',
        'order' => 5,
        'nullable' => '0',
        'required' => '1',
        'show_in_table' => '0',
        'show_in_form' => '1',
    ]

];